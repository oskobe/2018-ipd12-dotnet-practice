﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LambdaTest
{
    class Program
    {
        static void Main(string[] args)
        {
            int factor = 2;
            Func<int, int> multiplier = n => n * factor;
            factor = 3;
            Console.WriteLine(multiplier(3)); // 6

            int outerVariable = 0;
            Func<int> myLambda = () => outerVariable++;
            Console.WriteLine(myLambda()); // 0
            Console.WriteLine(myLambda()); // 1
            Console.WriteLine(outerVariable); // 2


            Console.ReadLine();
        }
    }
}
