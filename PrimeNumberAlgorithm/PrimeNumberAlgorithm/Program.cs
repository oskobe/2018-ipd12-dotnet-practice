﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrimeNumberAlgorithm
{
    class Program
    {
        static void Main(string[] args)
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();
            List<int> n100prime = getPrimeNumbers(5000);
            foreach (int i in n100prime)
            {
                Console.Write(i + " ");
            }
            sw.Stop();

            Console.WriteLine();
            Console.WriteLine("Total time: " + sw.ElapsedMilliseconds.ToString());

            Console.WriteLine();

            Stopwatch sw2 = new Stopwatch();
            int n100prime2;
            sw2.Start();

            for (int i = 1; i <= 5000; i++)
            {
                n100prime2 = getPrimeNumber(i);
                Console.Write(n100prime2 + " ");
            }
            
            sw2.Stop();

            Console.WriteLine();
            Console.WriteLine("Total time: " + sw2.ElapsedMilliseconds.ToString());

            Console.ReadLine();
        }

        public static List<int> getPrimeNumbers(int numOfPrimeWanted)
        {
            List<int> list = new List<int>();
            list.Add(2);
            int currentNumber = 2;
            bool notPrime;
            int primeFound = 1;

            while (primeFound < numOfPrimeWanted)
            {
                notPrime = false;
                currentNumber++;

                foreach (int i in list)
                {
                    if (i > Math.Sqrt(currentNumber))
                    {
                        break;
                    }
                    if (currentNumber % i == 0)
                    {
                        notPrime = true;
                        break;
                    }
                }
                if (!notPrime)
                {
                    list.Add(currentNumber);
                    primeFound++;
                }
            }

            return list;
            //return currentNumber;

        }

        public static int getPrimeNumber(int pos)
        {
            int number = 2;
            int count = 0;
            while (true)
            {
                if (checkIfPrimeNumber(number))
                {
                    count++;
                    if (count == pos)
                    {
                        return number;
                    }
                }
                number++;
            }
        }
        
        public static bool checkIfPrimeNumber(int num)
        {
            for (int i = 2; i <= Math.Sqrt(num); i++)
            {
                if (num % i == 0)
                {
                    return false;
                }
            }
            return true;
        }
    }
}
