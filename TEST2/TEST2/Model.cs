﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TEST2
{
    public class BlogContext : DbContext
    {
        public BlogContext() : base(@"Data Source = (LocalDB)\MSSQLLocalDB;AttachDbFilename=E:\Jing\IPD12-DotNet\TEST2\TEST2.mdf;Integrated Security = True; Connect Timeout = 30")
        {
        }

        public DbSet<Blog> Blogs { get; set; }
        public DbSet<Post> Posts { get; set; }
    }

    public class Blog
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public virtual List<Post> Posts { get; set; }
    }

    public class Post
    {
        public int BlogId { get; set; }
        public int Id { get; set; }

        public virtual Blog Blog { get; set; }

        
        [MaxLength(200)]
        public string Title { get; set; }

        
    }

}


