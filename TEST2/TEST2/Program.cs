﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TEST2
{
    class Program
    {
        static void Main(string[] args)
        {
            using (BlogContext ctx = new BlogContext())
            {
                Blog b = new Blog { Name = "ado.net blog" };
                ctx.Blogs.Add(b);
                

                var x = ctx.Blogs.Find(2);
                x.Posts.Add(new Post { Title = "3333", BlogId = 4 });

                ctx.Posts.Add(new Post { Title = "dddd", BlogId = 4});
                ctx.SaveChanges();

            }
            
        }
    }
}
