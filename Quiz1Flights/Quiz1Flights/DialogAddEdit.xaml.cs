﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Quiz1Flights
{
    /// <summary>
    /// Interaction logic for DialogAddEdit.xaml
    /// </summary>
    public partial class DialogAddEdit : Window
    {
        Flight currentFlight = null;

        public DialogAddEdit(Flight flight)
        {
            InitializeComponent();
            currentFlight = flight;
            InitializeDialog();
        }

        private void InitializeDialog()
        {
            cbType.ItemsSource = Enum.GetNames(typeof(Flight.FlightType));
            // Case: New
            if (currentFlight == null)
            {
                cbType.SelectedIndex = 0;
                this.Title = "Add New Flight";
            }
            // Case: Edit
            else
            {
                this.Title = "Edit Flight";
                lblId.Content = currentFlight.Id;
                dpOnDay.SelectedDate = currentFlight.OnDay;
                tbFromCode.Text = currentFlight.FromCode;
                tbToCode.Text = currentFlight.ToCode;
                cbType.SelectedIndex = cbType.Items.IndexOf(currentFlight.Type);
                sldPassengers.Value = currentFlight.Passengers;

            }

        }

        private void sldPassengers_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            lblPassengers.Content = sldPassengers.Value;
        }

        private void btSave_Click(object sender, RoutedEventArgs e)
        {
            DateTime? onDay = dpOnDay.SelectedDate;
            // Did not choose a date
            if (!onDay.HasValue)
            {
                MessageBox.Show("Plese choose a date.", "Input Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            string fromCode = tbFromCode.Text.Trim();
            // From code check
            int length = fromCode.Length;
            if (length > 5 || length < 3)
            {
                MessageBox.Show("From Code could only be 3-5 characters long.", "Input Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            string toCode = tbToCode.Text.Trim();
            // From code check
            length = toCode.Length;
            if (length > 5 || length < 3)
            {
                MessageBox.Show("To Code could only be 3-5 characters long.", "Input Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            // Did not set passengers number
            if (sldPassengers.Value == 0)
            {
                {
                    MessageBox.Show("Please choose passenger number.", "Input Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
            }

            // New
            if (currentFlight == null)
            {
                Flight flight = new Flight { OnDay = (DateTime)onDay, FromCode = fromCode, ToCode = toCode, Type = cbType.SelectedItem.ToString(), Passengers = (int)sldPassengers.Value };
                Globals.ctx.Flights.Add(flight);
            }
            // Update
            else
            {
                // Tested Update OK | Don't need to select the current flight again
                currentFlight.OnDay = (DateTime)onDay;
                currentFlight.FromCode = fromCode;
                currentFlight.ToCode = toCode;
                currentFlight.Type = cbType.SelectedItem.ToString();
                currentFlight.Passengers = (int)sldPassengers.Value;
            }

            try
            {
                Globals.ctx.SaveChanges();
                this.DialogResult = true;
            }
            catch (Exception ex)
            {
                string errMsg = (currentFlight == null) ? "Flight adding error." : "Flight updating error.";
                MessageBox.Show("Flight adding error.", "Database Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

        }
    }
}
