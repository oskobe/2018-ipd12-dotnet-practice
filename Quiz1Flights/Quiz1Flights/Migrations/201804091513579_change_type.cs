namespace Quiz1Flights.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class change_type : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Flights", "Type", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Flights", "Type", c => c.Int(nullable: false));
        }
    }
}
