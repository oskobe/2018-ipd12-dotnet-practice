namespace Quiz1Flights.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Flights",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        OnDay = c.DateTime(nullable: false),
                        FromCode = c.String(nullable: false, maxLength: 5),
                        ToCode = c.String(nullable: false, maxLength: 5),
                        Type = c.Int(nullable: false),
                        Passengers = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Flights");
        }
    }
}
