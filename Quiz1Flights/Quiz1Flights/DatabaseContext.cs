﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quiz1Flights
{
    class DatabaseContext : DbContext
    {
        public DatabaseContext() : base("name=FlightDatabaseEF")
        {
        }

        public virtual DbSet<Flight> Flights { get; set; }
    }
}
