﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quiz1Flights
{
    public class Flight
    {
        [Key]
        public long Id { get; set; }
        [Required]
        public DateTime OnDay { get; set; }
        [MaxLength(5)]
        [Required]
        public string FromCode { get; set; }
        [MaxLength(5)]
        [Required]
        public string ToCode { get; set; }
        [Required]
        public string Type { get; set; }
        [Required]
        public int Passengers { get; set; }

        public enum FlightType
        {
            Domestic,
            International,
            Private
        }
 
    }
}
