﻿using Microsoft.Win32;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Quiz1Flights
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        List<Flight> flightList = new List<Flight>();
        Flight flightSelected;

        public MainWindow()
        {
            InitializeComponent();

            ReLoadFlights();

        }

        private void ReLoadFlights()
        {
            flightList = (from f in Globals.ctx.Flights select f).ToList();
            lvFlights.ItemsSource = flightList;
            lblTotalFlights.Content = flightList.Count;
        }

        private void lvFlights_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            
        }

        private void lvFlightsItem_MouseRightButtonUp(object sender, MouseButtonEventArgs e)
        {
            ContextMenu myContextMenu = new ContextMenu(); ;

            MenuItem menuItem = new MenuItem();
            menuItem.Header = "Delete";
            myContextMenu.Items.Add(menuItem);
            menuItem.Click += new RoutedEventHandler(Delete_Flight_Click);

            myContextMenu.IsOpen = true;
        }

        private void Delete_Flight_Click(object sender, RoutedEventArgs e)
        {
            flightSelected = (Flight)lvFlights.SelectedItem;

            if (lvFlights.SelectedItem == null)
            {
                MessageBox.Show("Please select one flight.", "Selection Error", MessageBoxButton.OK,
                    MessageBoxImage.Warning);
                return;
            }

            MessageBoxResult result = MessageBox.Show("Are you sure to delete the flight: ID = " + flightSelected.Id + " ?", "Delete Confirmataion", MessageBoxButton.YesNo, MessageBoxImage.Question);

            if (result == MessageBoxResult.Yes)
            {

                Globals.ctx.Flights.Remove(flightSelected);
                
                try
                {
                    Globals.ctx.SaveChanges();
                    ReLoadFlights();
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Flight deletion error.", "Database Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
            }
        }

        private void lvFlightsItem_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            flightSelected = (Flight)lvFlights.SelectedItem;

            if (lvFlights.SelectedItem == null)
            {
                MessageBox.Show("Please select one flight.", "Selection Error", MessageBoxButton.OK,
                    MessageBoxImage.Warning);
                return;
            }

            DialogAddEdit dlg = new DialogAddEdit(flightSelected);
            dlg.ShowDialog();
            if (dlg.DialogResult == true)
            {
                ReLoadFlights();
            }
        }
       
        private void miAdd_Click(object sender, RoutedEventArgs e)
        {
            DialogAddEdit dlg = new DialogAddEdit(null);
            dlg.ShowDialog();
            if (dlg.DialogResult == true)
            {
                ReLoadFlights();
            }
        }

        private void miExit_Click(object sender, RoutedEventArgs e)
        {
            System.Environment.Exit(1);
        }

        private void miSaveSelected_Click(object sender, RoutedEventArgs e)
        {
            flightSelected = (Flight)lvFlights.SelectedItem;

            if (lvFlights.SelectedItem == null)
            {
                MessageBox.Show("Please select one flight.", "Selection Error", MessageBoxButton.OK,
                    MessageBoxImage.Warning);
                return;
            }

            SaveFileDialog dlg = new SaveFileDialog();
 
            dlg.RestoreDirectory = true;
            dlg.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
            dlg.FileName = "Flights";

            Nullable<bool> result = dlg.ShowDialog();
            if (result == true)
            {
                FileStream fs = null;
                StreamWriter sw = null;

                try
                {
                    fs = new FileStream(dlg.FileName, FileMode.Append);
                    sw = new StreamWriter(fs);

                    string line = flightSelected.Id + "," + flightSelected.OnDay + "," + flightSelected.FromCode + "," +
                        flightSelected.ToCode + "," + flightSelected.Type + "," + flightSelected.Passengers;
                    sw.WriteLine(line);
                    MessageBox.Show("File was saved successfully.", "Success", MessageBoxButton.OK,
                        MessageBoxImage.Information);
                }
                catch(IOException ex)
                {
                    MessageBox.Show("File save failed.", "Failed", MessageBoxButton.OK,
                        MessageBoxImage.Error);
                }

                sw.Flush();
                sw.Close();
                fs.Close();
                
            }
                



        }
    }
}
