﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace CustomSandwich
{
    /// <summary>
    /// Interaction logic for CustomDialog.xaml
    /// </summary>
    public partial class CustomDialog : Window
    {
        public CustomDialog()
        {
            InitializeComponent();
        }

        private void btSave_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        private void Window_ContentRendered(object sender, EventArgs e)
        {

        }

        public List<string> Selection
        {
            get
            {
                // Bread
                List<string> selection = new List<string>();
                selection.Add(cbBread.Text);

                // Veggies
                List<string> veggiesList = new List<string>();
                if (ckbLettuce.IsChecked ?? false) veggiesList.Add(ckbLettuce.Content.ToString());
                if (ckbTomatoes.IsChecked ?? false) veggiesList.Add(ckbTomatoes.Content.ToString());
                if (ckbCucumbers.IsChecked ?? false) veggiesList.Add(ckbCucumbers.Content.ToString());
                selection.Add(string.Join(",", veggiesList));

                // Meat;
                string meat = "";
                if (rbChicken.IsChecked ?? false) meat = rbChicken.Content.ToString();
                if (rbTurky.IsChecked ?? false) meat = rbTurky.Content.ToString();
                if (rbTofu.IsChecked ?? false) meat = rbTofu.Content.ToString();
                selection.Add(meat);

                // Return
                return selection;
            }
        }

        public String TestStr
        {
            get
            {
                return "Dalian";
            }
        }
    }
}
