﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CustomSandwich
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void btMakeSandwich_Click(object sender, RoutedEventArgs e)
        {
            CustomDialog customSandwichDialog = new CustomDialog();
            if (customSandwichDialog.ShowDialog() == true)
            {
                List<string> selection = customSandwichDialog.Selection;
                tbBread.Text = selection[0];
                tbVeggies.Text = selection[1];
                tbMeat.Text = selection[2];

                String x = customSandwichDialog.TestStr;
                MessageBox.Show(x);
            }
        }
    }
}
