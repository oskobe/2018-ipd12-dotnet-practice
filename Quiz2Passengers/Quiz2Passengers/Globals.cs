﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quiz2Passengers
{
    class Globals
    {
        public static Database db;
        public const string BYNAME = "name";
        public const string BYPASSPORTNO = "passport";
        public const string BYDESTINATION = "destination";
        public const string BYDEPDATETIME = "depDateTime";
    }
}
