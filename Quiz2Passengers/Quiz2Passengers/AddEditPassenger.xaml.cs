﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Quiz2Passengers
{
    /// <summary>
    /// Interaction logic for AddEditPassenger.xaml
    /// </summary>
    public partial class AddEditPassenger : Window
    {
        private Passenger currentPassenger;

        public AddEditPassenger(Passenger passenger)
        {
            currentPassenger = passenger;

            InitializeComponent();

            if (currentPassenger == null)
            {
                btSave.Content = "Add new";
                btDelete.IsEnabled = false;
            }
            else
            {
                lblId.Content = passenger.Id + "";
                tbName.Text = passenger.Name;
                tbPassport.Text = passenger.Passport;
                tbDestination.Text = passenger.Destination;
                dpDepDate.SelectedDate = passenger.DepartureDateTime;
                cbDepTime.Text = passenger.DepartureDateTime.TimeOfDay.ToString().Substring(0, 5);
            }
        }

        private void btDelete_Click(object sender, RoutedEventArgs e)
        {
            Globals.db.DeletePassenger(currentPassenger.Id);
            DialogResult = true;
        }

        private void btCancle_Click(object sender, RoutedEventArgs e)
        {
            // By setting isCancel = true, don't need to set false => dialogResult
            //DialogResult = false;
        }

        private void btSave_Click(object sender, RoutedEventArgs e)
        {
            Passenger passenger = currentPassenger == null ? new Passenger() : currentPassenger;

            if (tbName.Text.Trim().Equals(""))
            {
                return;
            }

            if (tbName.Text.Length < 1 || tbName.Text.Length > 100)
            {
                MessageBox.Show("Name must be 1-100 long characters.", "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            passenger.Name = tbName.Text;

            if (tbPassport.Text.Trim().Equals(""))
            {
                return;
            }
            Regex rgx = new Regex(@"[A-Z]{2}[0-9]{8}$");
            if (!rgx.IsMatch(tbPassport.Text))
            {
                MessageBox.Show("Passport No. must be the format \"AB12345678\".", "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            passenger.Passport = tbPassport.Text;

            if (tbDestination.Text.Trim().Equals(""))
            {
                return;
            }
            if (tbDestination.Text.Length < 1 || tbDestination.Text.Length > 100)
            {
                MessageBox.Show("Destination must be 1-100 long characters.", "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            passenger.Destination = tbDestination.Text;

            if (dpDepDate.SelectedDate == null)
            {
                MessageBox.Show("Please choose a departure date", "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            passenger.DepartureDateTime = (DateTime)dpDepDate.SelectedDate;

            if (cbDepTime.SelectedValue == null)
            {
                MessageBox.Show("Please choose a departure time", "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            string[] hhmmss = cbDepTime.SelectedValue.ToString().Split(':');

            int addedMinutes = int.Parse(hhmmss[1]) * 60 + int.Parse(hhmmss[2]);
            passenger.DepartureDateTime = passenger.DepartureDateTime.Date.AddMinutes(addedMinutes);

            if (currentPassenger == null)
            // add new
            {
                Globals.db.AddPassenger(passenger);
            }
            else
            // update old
            {
                Globals.db.UpdatePassenger(passenger);
            }

            DialogResult = true;

        }
    }
}
