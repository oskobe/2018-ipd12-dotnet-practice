﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Quiz2Passengers
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        List<Passenger> cachePassengerList = new List<Passenger>();

        public MainWindow()
        {
            try
            {
                Globals.db = new Database();
                InitializeComponent();
                refreshPassengerList();

            }
            catch (SqlException e)
            {
                Console.WriteLine(e.StackTrace);
                MessageBox.Show("Error opening database connection: " + e.Message);
                Environment.Exit(1);
            }

        }

        private void refreshPassengerList()
        {
            cachePassengerList = Globals.db.GetAllPassengers();
            lvPassengers.ItemsSource = cachePassengerList;
        }

        private void btAdd_Click(object sender, RoutedEventArgs e)
        {
            AddEditPassenger dlg = new AddEditPassenger(null);
            if (dlg.ShowDialog() == true)
            {
                refreshPassengerList();
            }

        }

        private void btSortBy_Click(object sender, RoutedEventArgs e)
        {
            SortByDialog sbDialog = new SortByDialog();

            List<Passenger> passengerList = cachePassengerList;

            if (sbDialog.ShowDialog() == true)
            {
                if (sbDialog.Selection.Equals(Globals.BYNAME))
                {
                    var result = from p in passengerList orderby p.Name select p;
                    passengerList = result.ToList();
                }
                if (sbDialog.Selection.Equals(Globals.BYPASSPORTNO))
                {
                    var result = from p in passengerList orderby p.Passport select p;
                    passengerList = result.ToList();
                }
                if (sbDialog.Selection.Equals(Globals.BYDESTINATION))
                {
                    var result = from p in passengerList orderby p.Destination select p;
                    passengerList = result.ToList();
                }
                if (sbDialog.Selection.Equals(Globals.BYDEPDATETIME))
                {
                    var result = from p in passengerList orderby p.DepartureDateTime select p;
                    passengerList = result.ToList();
                }

                lvPassengers.ItemsSource = passengerList;
            }
        }

        private void LvPassengersItem_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            Passenger passenger = (Passenger)lvPassengers.SelectedItem;

            // USE XMAL markup to make sure event only occurs when the item was seleted

            AddEditPassenger dlg = new AddEditPassenger(passenger);
            if (dlg.ShowDialog() == true)
            {
                refreshPassengerList();
            }
        }

        private void tbSearch_TextChanged(object sender, TextChangedEventArgs e)
        {
            List<Passenger> passengerList = cachePassengerList;

            string word = tbSearch.Text;
            if (word != "")
            {
                var result = from p in passengerList where p.Name.Contains(word) || p.Destination.Contains(word) select p;
                passengerList = result.ToList();
            }

            lvPassengers.ItemsSource = passengerList;
        }

        
    }
}
