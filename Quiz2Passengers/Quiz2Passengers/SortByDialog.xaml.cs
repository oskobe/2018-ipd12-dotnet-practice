﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Quiz2Passengers
{
    /// <summary>
    /// Interaction logic for SortByDialog.xaml
    /// </summary>
    public partial class SortByDialog : Window
    {
        public SortByDialog()
        {
            InitializeComponent();
        }

        private void btApply_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }

        public string Selection
        {
            get
            {
                string selection = "";

                if (rbName.IsChecked??false)
                {
                    selection = Globals.BYNAME;
                }

                if (rbPassport.IsChecked ?? false)
                {
                    selection = Globals.BYPASSPORTNO;
                }

                if (rbDestination.IsChecked ?? false)
                {
                    selection = Globals.BYDESTINATION;
                }

                if (rbDepDateTime.IsChecked ?? false)
                {
                    selection = Globals.BYDEPDATETIME;
                }

                return selection;
            }
        }
    }
}
