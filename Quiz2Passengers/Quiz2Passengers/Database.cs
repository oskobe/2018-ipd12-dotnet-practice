﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quiz2Passengers
{
    class Database
    {
        SqlConnection conn;

        public Database()
        {
            string dbConnectString = @"Data Source=myjac.database.windows.net;Initial Catalog=myJacDB;Persist Security Info=True;User ID=jing;Password=1qaz@WSX";

            conn = new SqlConnection(dbConnectString);
            conn.Open();
        }

        public List<Passenger> GetAllPassengers()
        {
            List<Passenger> list = new List<Passenger>();
            SqlCommand command = new SqlCommand("SELECT * FROM passengers", conn);

            using (SqlDataReader reader = command.ExecuteReader())
            {
                while (reader.Read())
                {
                    int id = (int)reader["Id"];
                    string name = (string)reader["Name"];
                    string passport = (string)reader["Passport"];
                    string destination = (string)reader["Destination"];
                    DateTime departureDateTime = (DateTime)reader["DepartureDateTime"];

                    Passenger Passenger = new Passenger() { Id = id, Name = name, Passport = passport, Destination = destination, DepartureDateTime = departureDateTime };
                    list.Add(Passenger);
                }
                return list;
            }

        }

        public void AddPassenger(Passenger Passenger)
        {
            using (SqlCommand insertCommand = new SqlCommand("INSERT INTO passengers (Name, Passport, Destination, DepartureDateTime) VALUES (@name, @passport, @destination, @departureDateTime)", conn))
            {
                insertCommand.Parameters.AddWithValue("@name", Passenger.Name);
                insertCommand.Parameters.AddWithValue("@passport", Passenger.Passport);
                insertCommand.Parameters.AddWithValue("@destination", Passenger.Destination);
                insertCommand.Parameters.AddWithValue("@departureDateTime", Passenger.DepartureDateTime);
               
                insertCommand.ExecuteNonQuery();
            }

        }

        public void UpdatePassenger(Passenger Passenger)
        {
            using (SqlCommand insertCommand = new SqlCommand("UPDATE passengers SET Name = @name, Passport = @passport, Destination = @destination, DepartureDateTime = @departureDateTime WHERE Id = @id", conn))
            {
                insertCommand.Parameters.AddWithValue("@name", Passenger.Name);
                insertCommand.Parameters.AddWithValue("@passport", Passenger.Passport);
                insertCommand.Parameters.AddWithValue("@destination", Passenger.Destination);
                insertCommand.Parameters.AddWithValue("@departureDateTime", Passenger.DepartureDateTime);
                insertCommand.Parameters.AddWithValue(@"id", Passenger.Id);

                insertCommand.ExecuteNonQuery();
            }

        }

        public void DeletePassenger(int id)
        {
            using (SqlCommand insertCommand = new SqlCommand("DELETE FROM passengers WHERE Id = @id", conn))
            {
                insertCommand.Parameters.AddWithValue(@"id", id);
                insertCommand.ExecuteNonQuery();
            }

        }
    }
}
