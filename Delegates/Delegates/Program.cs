﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Delegates
{
    class Program
    {
        static void Main(string[] args)
        {
            PhotoProcessor processor = new PhotoProcessor();
            var filter = new PhotoFilters();
            PhotoProcessor.PhotoFilterHandler filterHander = filter.ApplyBrightness;
            filterHander += filter.ApplyContrast;
            filterHander += RemoveRedEye;

            processor.Process("photo.jpg", filterHander);

            Console.Write("Press any key to continue ... ");
            Console.ReadLine();
        }

        static void RemoveRedEye(Photo photo)
        {
            Console.WriteLine("Apply Remove Red Eye");
        }
    }
}
