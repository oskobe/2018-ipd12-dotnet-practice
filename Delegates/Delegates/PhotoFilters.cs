﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Delegates
{
    class PhotoFilters
    {
        public void ApplyBrightness(Photo photo)
        {
            Console.WriteLine("Aply Brightness");
        }

        public void ApplyContrast(Photo photo)
        {
            Console.WriteLine("Aply Contrast");
        }

        public void ApplyResize(Photo photo)
        {
            Console.WriteLine("Aply Resize");
        }
    }
}
