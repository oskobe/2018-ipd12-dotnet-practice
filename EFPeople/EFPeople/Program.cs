﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EFPeople
{
    class Program
    {
        static void Main(string[] args)
        {
            Random rand = new Random();
            Person p = new Person { Name = "Jerry", Age = rand.Next(1, 150) };

            using (DatabaseContext ctx = new DatabaseContext())
            {
                // insert
                ctx.People.Add(p);  // Persist schedule person to be inserted into database table
                ctx.SaveChanges(); // Flush - force all scheduled operations to be carried out in database

                Console.WriteLine("Person persisted");

                // reattach and delete
                Person p2 = new Person { PersonId = 3 };
                Console.WriteLine("Deletion before attaching, P({0}): {1}, {2}", p2.PersonId, p2.Name, p2.Age);
                ctx.People.Attach(p2);
                Console.WriteLine("Deletion after attaching, P({0}): {1}, {2}", p2.PersonId, p2.Name, p2.Age);
                ctx.People.Remove(p2);
                if (ctx.Entry(p2).State != System.Data.Entity.EntityState.Deleted)
                {
                    ctx.SaveChanges();
                }

                // update (select then update)
                var peop = (from r in ctx.People where r.PersonId == 2 select r).ToList();
                if (peop.Count < 1)
                {
                    Console.WriteLine("Record for update not found");
                } else
                {
                    Person ppp = peop[0];
                    ppp.Age = rand.Next(1, 150); // entity is tracked/attached, meaning this will cause an update to be scheduled
                    ctx.SaveChanges();
                }

                // select
                var people = (from r in ctx.People select r).ToList();

                foreach (var pp in people)
                {
                    Console.WriteLine("P({0}): {1}, {2}", pp.PersonId, pp.Name, pp.Age);
                }

                


            }
            Console.ReadLine();
        }

        
    }
}
