﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TeamMembers
{
    class Program
    {
        static void Main(string[] args)
        {
            const string PLAYSIN = " plays in: ";
            string allTeam = "";
            Dictionary<string, List<string>> list = ReadAndParseFile("../../input.txt");

            foreach(KeyValuePair<string, List<string>> kvp in list)
            {
                // print memeber name
                Console.Write(kvp.Key + PLAYSIN);
                // get all team info
                allTeam = String.Join(", ", kvp.Value);
                // print team list
                Console.WriteLine(allTeam);
            }

            Console.ReadLine();
        }

        public static Dictionary<string, List<string>> ReadAndParseFile(string path)
        {
            StreamReader sr = new StreamReader(path, Encoding.Default);
            string line;
            string[] lineArr;
            Dictionary<string, List<string>> list = new Dictionary<string, List<string>>();
            string teamName = "";

            while ((line = sr.ReadLine()) != null)
            {
                Console.WriteLine(line);
                lineArr = line.Split(new string[] {":", ","}, StringSplitOptions.None);
                for (int i = 0; i < lineArr.Count(); i++)
                {
                    // the first part, team name
                    if (i == 0)
                    {
                        teamName = lineArr[i];
                        continue;
                    }
                    // new member
                    if (!list.ContainsKey(lineArr[i]))
                    {
                        list.Add(lineArr[i], new List<string> {teamName});
                    }
                    // add team for existing member
                    else {
                        list[lineArr[i]].Add(teamName);
                    }
                }
                
            }
            Console.WriteLine();
            return list;
        }
    }
}
