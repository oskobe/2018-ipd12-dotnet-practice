﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinqSortPractice
{
    class Fruit
    {
        public string name;
        public double weight;
    }

   

    class Program
    {
        static void Main(string[] args)
        {
            List<Fruit> list = new List<Fruit>();
            list.Add(new Fruit() { name = "Peach", weight = 5.66 });
            list.Add(new Fruit() { name = "Banada", weight = 9.99 });
            list.Add(new Fruit() { name = "Orange", weight = 4.54 });
            list.Add(new Fruit() { name = "Banada", weight = 1.76 });
            list.Add(new Fruit() { name = "Apple", weight = 3.78 });

            foreach (Fruit f in list)
            {
                Console.WriteLine("{0} is {1} kg", f.name, f.weight);
            }

            Console.WriteLine("After sort");

            var sortedList = from f in list orderby f.name, f.weight select f;

            foreach(Fruit f in sortedList)
            {
                Console.WriteLine("{0} is {1} kg", f.name, f.weight);
            }

            Console.ReadLine();

        }
    }
}
