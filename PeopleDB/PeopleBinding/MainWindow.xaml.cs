﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PeopleDB
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        List<Person> peopleList = new List<Person>();
        int idxSelectedPerson = -1;
        Person selectedPerson = null;
        const string CENTIMETER = " CM";
        Database db = null;

        public MainWindow()
        {
            InitializeComponent();

            lvPeople.ItemsSource = peopleList;
            db = new Database();
            LoadPeopleList();
        }

        private void LoadPeopleList()
        {
            if (lvPeople.Items.Count != 0)
            {
                lvPeople.Items.Clear();
            }

            peopleList = db.GetAllPeople();
           // lvPeople.ItemsSource = peopleList;
            lvPeople.Items.Refresh();
        }
        
        private void btAdd_Click(object sender, RoutedEventArgs e)
        {
            string name = tbName.Text;
            string ageStr = tbAge.Text;
            int age;
            double height = sldHeight.Value;
            
            if (name.Trim().Equals(""))
            {
                return;
            }
            if (!int.TryParse(ageStr.Trim(), out age))
            {
                MessageBox.Show("Age must be integer number.", "Input error", 
                    MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            
            try
            {
                Person person = new Person(0, name, age, height);
                db.AddPerson(person);
                LoadPeopleList();
            }
            catch (InvalidDataException ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Input error",
                    MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            
        }
        
        private void btUpdate_Click(object sender, RoutedEventArgs e)
        {
            string name = tbName.Text;
            string ageStr = tbAge.Text;
            int age;
            Person person = null;
            double height = sldHeight.Value;

            if (lblId.Content.Equals("..."))
            {
                MessageBox.Show("Please choose one person.", "Selection error",
                    MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            if (name.Trim().Equals(""))
            {
                return;
            }
            if (!int.TryParse(ageStr.Trim(), out age))
            {
                MessageBox.Show("Age must be integer number.", "Input error",
                    MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            // Do not use person object, just for valid the input data 
            try
            {
                person = new Person(selectedPerson.Id, name, age, height);
                db.UpdatePerson(person);
                LoadPeopleList();
            }
            catch (InvalidDataException ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Input error",
                    MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }


        }

        private void lvPeople_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //ListViewItem lvi = (ListViewItem)lvPeople.SelectedItem;
            selectedPerson = (Person)lvPeople.SelectedItem;
            if (selectedPerson != null)
            {
                lblId.Content = selectedPerson.Id;
                tbName.Text = selectedPerson.Name;
                tbAge.Text = selectedPerson.Age.ToString();
                sldHeight.Value = selectedPerson.Height;
                lblHeight.Content = selectedPerson.Height + CENTIMETER;
                idxSelectedPerson = lvPeople.SelectedIndex;
            } else
            {
                lblId.Content = "...";
            }
            
        }

        private void DeletePerson_Click(object sender, RoutedEventArgs e)
        {
            if (lvPeople.SelectedItem == null)
            {
                MessageBox.Show("Please select one person.", "Selection Error", MessageBoxButton.OK,
                    MessageBoxImage.Warning);
                return;
            }

            MessageBoxResult result = MessageBox.Show("Are you sure to delete the person: " + selectedPerson.Id + "#  \"" + selectedPerson.Name + "\".", "Delete confirmataion", MessageBoxButton.YesNo, MessageBoxImage.Question);

            if (result == MessageBoxResult.Yes)
            {
                db.DeletePerson(selectedPerson.Id);
            }
        }

        private void sldHeight_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (lblHeight != null)
            {
                lblHeight.Content = string.Format("{0:0.0}", sldHeight.Value) + CENTIMETER;
            }
                
        }
    }
}
