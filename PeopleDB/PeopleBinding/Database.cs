﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PeopleDB
{
    class Database
    {
        SqlConnection conn;

        public Database()
        {
            string dbConnectString = "Data Source=myjac.database.windows.net;Initial Catalog=myJacDB;Persist Security Info=True;User ID=jing;Password=1qaz@WSX";

            conn = new SqlConnection(dbConnectString);
            conn.Open();
        }

        public List<Person> GetAllPeople()
        {
            List<Person> list = new List<Person>();
            SqlCommand command = new SqlCommand("SELECT * FROM people", conn);

            using (SqlDataReader reader = command.ExecuteReader())
            {
                while (reader.Read())
                {
                    Person person = new Person((int)reader[0], (string)reader[1], (int)reader[2], (double)reader[3]);
                    list.Add(person);
                }
                return list;
            }
            
        }

        public void AddPerson(Person person)
        {
            using (SqlCommand insertCommand = new SqlCommand("INSERT INTO people (name, age, height) VALUES (@name, @age, @height)", conn))
            {
                insertCommand.Parameters.AddWithValue(@"name", person.Name);
                insertCommand.Parameters.AddWithValue(@"age", person.Age);
                insertCommand.Parameters.AddWithValue(@"height", person.Height);
                insertCommand.ExecuteNonQuery();
            }

        }

        public void UpdatePerson(Person person)
        {
            using (SqlCommand insertCommand = new SqlCommand("UPDATE people SET name = @name, age = @age, height = @height WHERE id = @id", conn))
            {
                insertCommand.Parameters.AddWithValue(@"name", person.Name);
                insertCommand.Parameters.AddWithValue(@"age", person.Age);
                insertCommand.Parameters.AddWithValue(@"height", person.Height);
                insertCommand.Parameters.AddWithValue(@"id", person.Id);
                insertCommand.ExecuteNonQuery();
            }

        }

        public void DeletePerson(int id)
        {
            using (SqlCommand insertCommand = new SqlCommand("DELETE FROM people WHERE id = @id", conn))
            {
                insertCommand.Parameters.AddWithValue(@"id", id);
                insertCommand.ExecuteNonQuery();
            }

        }
    }
}
