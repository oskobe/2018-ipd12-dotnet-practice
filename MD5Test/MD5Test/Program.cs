﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;


namespace MD5Test
{
    class Program
    {
        static void Main(string[] args)
        {
            string source = "Happy Birthday!";

            string hash = GetMd5Hash(source);
            Console.WriteLine("The MD5 hash of " + source + " is: " + hash);

            Console.WriteLine("Verifying the hash ...");

            if (VerifyMd5Hash(source, hash))
            {
                Console.WriteLine("The hashes are the same.");
            }
            else
            {
                Console.WriteLine("The hashes are not same.");
            }

            Console.ReadKey();
        }

        /// <summary>
        /// 获取一个字符串的32位16进制字符串格式MD5码
        /// </summary>
        /// <param name="input">原字符串</param>
        /// <returns></returns>
        static string GetMd5Hash(string input)
        {
            MD5CryptoServiceProvider md5Hasher = new MD5CryptoServiceProvider();

            byte[] inputBytes = Encoding.Default.GetBytes(input);

            byte[] data = md5Hasher.ComputeHash(inputBytes);

            StringBuilder sBuilder = new StringBuilder();

            //将data中的每个字符都转换为16进制的
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            return sBuilder.ToString();
        }

        /// <summary>
        /// 验证Md5 hash
        /// </summary>
        /// <param name="input">原字符串</param>
        /// <param name="hash">原字符串的md5码</param>
        /// <returns></returns>
        static bool VerifyMd5Hash(string input, string hash)
        {
            string hashOfInput = GetMd5Hash(input);

            StringComparer comparer = StringComparer.OrdinalIgnoreCase;

            if (0 == comparer.Compare(hashOfInput, hash))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}