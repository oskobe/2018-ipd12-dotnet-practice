﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace TempConv
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            lblCelsius.Content = "Jerry is here";
        }

        private void btConvert_Click(object sender, RoutedEventArgs e)
        {
            double cel;
            if (double.TryParse(tbCelsius.Text, out cel))
            {
                double fah = cel * 9 / 5 + 32;
                lblFahrenheit.Content = string.Format("{0:0.00} F", fah);
            }
        }
    }
}
