﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quiz1Rummers
{
    class Runner
    {
        string _name;
        double _avgTime;
        List<double> _runtimesList = new List<double>();

        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                if (value.Length >= 2 && value.Length <= 20)
                {
                    _name = value;
                }
                else
                {
                    throw new InvalidDataException("Name must be 2-20 characters long");
                }
            }
        }

        public double AvgTime
        {
            get
            {
                return _avgTime;
            }

            set
            {
                if (value >= 0)
                {
                    _avgTime = value;
                }
                else
                {
                    throw new InvalidDataException("Average time must be equal or great than 0");
                }
            }
        }

        public Runner(string name, double avgTime, List<double> list)
        {
            Name = name;
            AvgTime = avgTime;
            _runtimesList = list;
        }

        public List<double> GetRunTimeList()
        {
            return _runtimesList;
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            // Create dictionary to save result
            Dictionary<string, List<double>> runnersTimesDict = new Dictionary<string, List<double>>();
            List<Runner> runnerList = new List<Runner>();

            try
            {
                // Read result from file
                string[] lines = File.ReadAllLines(@"runners.txt");
                double runTime = 0;
                string name = "";

                // Parse data from file
                foreach(string line in lines)
                {
                    bool isDouble = double.TryParse(line, out runTime);
                    // The line is name
                    if (!isDouble)
                    {
                        name = line;
                        runnersTimesDict.Add(name, new List<double>());
                    }
                    // The line is time
                    else
                    {
                        runnersTimesDict[name].Add(runTime);
                    }
                }

                double totalTimeOfAllRunner = 0;
                double totalTimeOfEachRunner = 0;
                int totalTimesOfAllrunner = 0;
                double theFastestTime = 100000;

                // Each runner
                foreach (KeyValuePair<string, List<double>> kvp in runnersTimesDict)
                {
                    totalTimeOfEachRunner = 0;
                    // Each time
                    foreach (double time in kvp.Value)
                    {
                        totalTimeOfEachRunner += time;
                        if (time < theFastestTime)
                        {
                            theFastestTime = time;
                        }
                    }

                    totalTimeOfAllRunner += totalTimeOfEachRunner;
                    totalTimesOfAllrunner += kvp.Value.Count;
                    double avgTimeOfEachRunner = totalTimeOfEachRunner / kvp.Value.Count;
                    runnerList.Add(new Runner(kvp.Key, avgTimeOfEachRunner, kvp.Value));

                }

                double avgTimeOfAllRunner = totalTimeOfAllRunner / totalTimesOfAllrunner;

                // Print report
                Console.WriteLine("Average run time for all runners was {0:0.00}", avgTimeOfAllRunner);
                Console.WriteLine("The fastest ran was {0:0.00}", theFastestTime);

                foreach (Runner runner in runnerList)
                {
                    Console.WriteLine("{0} ran {1} time(s)", runner.Name, runner.GetRunTimeList().Count);
                }

                foreach(Runner runner in runnerList)
                {
                    Console.WriteLine("{0}'s average is {1:0.00}", runner.Name, runner.AvgTime);
                }

                Console.WriteLine("There were {0} runners in total", runnerList.Count);

                // Part 2

                var sortedRunnerList = from runner in runnerList orderby runner.Name select runner;

                Console.WriteLine();
                Console.WriteLine("----- Sorted runner list -----");
                foreach (Runner runner in sortedRunnerList)
                {
                    Console.WriteLine("{0}  {1:0.00}", runner.Name, runner.AvgTime);
                }


                Console.ReadLine();
                
            }
            catch (IOException ex)
            {
                Console.WriteLine("File fetching error: \n" + ex.Message);
            }
            
        }
    }
}
