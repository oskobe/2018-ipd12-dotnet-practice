﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TEST4
{
    public class BlogContext : DbContext
    {
        public BlogContext() : base(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=E:\Jing\IPD12-DotNet\TEST4\TEST4.mdf;Integrated Security=True;Connect Timeout=30")
        {
        }

        public DbSet<Blog> Blogs { get; set; }
        public DbSet<Post> Posts { get; set; }
        public DbSet<User> Users { get; set; }
    }

    public class Blog
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public virtual List<Post> Posts { get; set; }
    }

    public class Post
    {
        public int Id { get; set; }
        [MaxLength(200)]
        public string Title { get; set; }

        public int Xxx { get; set; }

        public int Content { get; set; }

        public virtual Blog Blog { get; set; }


    }
    public class User
    {      
        [Key]
       public int XxId { get; set; }
        public string Title { get; set; }
    }
}
