﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DelegateThis
{
    class Program
    {
        static void PrintSimple(string line)
        {
            Console.WriteLine("SIMPLE: " + line);
        }

        static void PrintFancy(string line)
        {
            Console.WriteLine("!!!!!!!!!!!!!!!!! {0} !!!!!!!!!!!!!!!!! {1}" , line, line.Length);
        }

        static void PrintToFile(string line)
        {
            File.AppendAllText("output.txt", line + "\n");
        }

        // declare the type of reference to a method - the signature of the method
        delegate void PrinterMethodType(string s);

        static void Main(string[] args)
        {
            PrinterMethodType printer = null;

            printer = PrintSimple;
            //printer += PrintFancy;
            printer += PrintToFile;
            printer -= PrintFancy;

            printer("Hello, via delegate");
            Console.ReadLine();
        }
    }
}
