﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PeopleBinding
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        ObservableCollection<Person> peopleList = new ObservableCollection<Person>();
        int idxSelectedPerson = -1;
        Person selectedPerson = null;

        public MainWindow()
        {
            InitializeComponent();
            InitializeList();
        }

        private void InitializeList()
        {
            lvPeople.ItemsSource = peopleList;

            peopleList.Add(new Person("David Lee", 29));
            peopleList.Add(new Person("Janet White", 29));
            peopleList.Add(new Person("Daniel Jakson", 39));
            peopleList.Add(new Person("Smith Black", 39));

        }

        public class Person:INotifyPropertyChanged
        {
            private int _id;
            private string _name;
            private int _age;
            private static int _idCounter = 10000;
            private const string ERROR_MESSAGE_NAME = "Name must be 2-50 characters long.";
            private const string ERROR_MESSAGE_AGE = "Age must be 1-150.";

            public int Id
            {
                get
                {
                    return _id;
                }
               
            }

            public string Name
            {
                get
                {
                    return _name;
                }
                set
                {
                    if (value.Length < 2 || value.Length > 50)
                    {
                        throw new InvalidDataException(ERROR_MESSAGE_NAME);
                    }
                    _name = value;
                    OnNotifyPropertyChanged();
                }
            }

            public int Age
            {
                get
                {
                    return _age;
                }
                set
                {
                    if (value < 1 || value > 150)
                    {
                        throw new InvalidDataException(ERROR_MESSAGE_AGE);
                    }
                    _age = value;
                    //OnNotifyPropertyChanged();
                    NotifyPropertyChanged();
                }
            }

            public Person(string name, int age) 
            {
                Name = name;
                Age = age;
                _id = GetUniqId();
            }

            public Person()
            {
            }
            /*
            protected virtual void OnNotifyPropertyChanged([System.Runtime.CompilerServices.CallerMemberName] String propertyName = "")
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
            }
            */
            public event PropertyChangedEventHandler PropertyChanged;
            private void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
            {
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
                }
            }
            /*
            protected void OnPropertyChanged(string name)
            {
                PropertyChangedEventHandler handler = PropertyChanged;
                if (handler != null)
                {
                    handler(this, new PropertyChangedEventArgs(name));
                }
            }
            
            public void OnPropertyChanged(PropertyChangedEventArgs e)
            {
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, e);
                }
            }*/


            private static int GetUniqId()
            {
                return ++_idCounter;
            }
        }

        private void btAdd_Click(object sender, RoutedEventArgs e)
        {
            string name = tbName.Text;
            string ageStr = tbAge.Text;
            int age;
            
            if (name.Trim().Equals(""))
            {
                return;
            }
            if (!int.TryParse(ageStr.Trim(), out age))
            {
                MessageBox.Show("Age must be integer number.", "Input error", 
                    MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            
            try
            {
                Person person = new Person(name, age);
                peopleList.Add(person);
            }
            catch (InvalidDataException ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Input error",
                    MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            
        }

        private void btUpdate_Click(object sender, RoutedEventArgs e)
        {
            string name = tbName.Text;
            string ageStr = tbAge.Text;
            int age;
            Person person = null;

            if (lblId.Content.Equals("..."))
            {
                MessageBox.Show("Please choose one person.", "Selection error",
                    MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            if (name.Trim().Equals(""))
            {
                return;
            }
            if (!int.TryParse(ageStr.Trim(), out age))
            {
                MessageBox.Show("Age must be integer number.", "Input error",
                    MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            // Do not use person object, just for valid the input data 
            try
            {
                person = new Person(name, age);
                //peopleList.Add(person);
            }
            catch (InvalidDataException ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Input error",
                    MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            // Do not need to refresh, but the id would be changed
            //peopleList.RemoveAt(idxSelectedPerson);
            //peopleList.Insert(idxSelectedPerson, person);

            peopleList[idxSelectedPerson].Name = name;
            peopleList[idxSelectedPerson].Age = age;
            //lvPeople.Items.Refresh();

        }

        private void lvPeople_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //ListViewItem lvi = (ListViewItem)lvPeople.SelectedItem;
            selectedPerson = (Person)lvPeople.SelectedItem;
            if (selectedPerson != null)
            {
                lblId.Content = selectedPerson.Id;
                tbName.Text = selectedPerson.Name;
                tbAge.Text = selectedPerson.Age.ToString();
                idxSelectedPerson = lvPeople.SelectedIndex;
            }
            
        }

        private void DeletePerson_Click(object sender, RoutedEventArgs e)
        {
            if (lvPeople.SelectedItem == null)
            {
                MessageBox.Show("Please select one person.", "Selection Error", MessageBoxButton.OK,
                    MessageBoxImage.Warning);
                return;
            }

            MessageBoxResult result = MessageBox.Show("Are you sure to delete the person: " + selectedPerson.Id + "#  \"" + selectedPerson.Name + "\".", "Delete confirmataion", MessageBoxButton.YesNo, MessageBoxImage.Question);

            if (result == MessageBoxResult.Yes)
            {
                peopleList.RemoveAt(idxSelectedPerson);
            }
        }
    }
}
