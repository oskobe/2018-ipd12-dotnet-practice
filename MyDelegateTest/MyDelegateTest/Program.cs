﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyDelegateTest
{
    class Program
    {
        public delegate void MyDelegate(string name);

        public static void MyDelegateFunc(string name)
        {
            Console.WriteLine("Hello, {0}", name);
        }

        public static void MyDelegateFunc2(string name)
        {
            Console.WriteLine("Hello, {0}, welcome to my class", name);
        }

        static void Main(string[] args)
        {
            MyDelegate md;
            md = new MyDelegate(MyDelegateFunc);
            md("Jing Wang");
            md += MyDelegateFunc2;
            md("Jing Wang");
            Console.ReadLine();
        }
    }
}


