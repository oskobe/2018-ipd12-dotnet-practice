﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelloWorld
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World");
            Console.WriteLine("What's your name?");
            string name = Console.ReadLine();
            if (name == "Santa")
            {
                Console.WriteLine("Can't believe it is Santa!");
            }
            Console.WriteLine("Hello {0}, nice to meet you!", name);
            Console.ReadLine();
        }
    }
}
