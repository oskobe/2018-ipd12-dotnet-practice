﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ScoopSelector
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private System.Collections.ObjectModel.ObservableCollection<Selector> items;
        public MainWindow()
        {
            InitializeComponent();
            items = new System.Collections.ObjectModel.ObservableCollection<Selector>();


            items.Add(new Selector() { Name = "Vanilla" });
            items.Add(new Selector() { Name = "Chololate" });
            items.Add(new Selector() { Name = "Strewberry" });
            items.Add(new Selector() { Name = "Peach" });
            lvFlavours.ItemsSource = items;
        }

        private void miDeletelvSelectedItem_Click(object sender, RoutedEventArgs e)
        {
            if (lvSelected.SelectedItems.Count <= 0)
            {
                MessageBox.Show("No scoop was selected.", "Warning message");
                return;
            }
            lvSelected.Items.RemoveAt(lvSelected.SelectedIndex);
        }

        private void miClearAll_Click(object sender, RoutedEventArgs e)
        {
            var result = MessageBox.Show("Do you want to delete all data?", "Delete All", MessageBoxButton.YesNo, MessageBoxImage.Question, MessageBoxResult.No);
            if (result == MessageBoxResult.Yes)
            {
                lvSelected.Items.Clear();
            }

        }

        private void btAdd_Click(object sender, RoutedEventArgs e)
        {
            foreach (var item in lvFlavours.SelectedItems)
            {
                lvSelected.Items.Add(item);
            }
        }

        private void btAddTolvScoopSelector_Click(object sender, RoutedEventArgs e)
        {
            string newScoop = tbAddScoop.Text;
            if (string.IsNullOrEmpty(newScoop))
            {
                MessageBox.Show("No Scoop was given!", "Message");
            }
            var selector = new Selector(newScoop);
            //items.Add(selector);
            tbAddScoop.Clear();
            //lvFlavours.Items.Refresh() goes to garbage haha
            //test
            items[0].Name = newScoop;
        }


    }
    public class Selector : System.ComponentModel.INotifyPropertyChanged
    {
        public Selector()
        {
        }

        public Selector(string name)
        {
            Name = name;
        }


        public override string ToString()
        {
            return Name;
        }

        private string name;
        public string Name
        {
            get => _name;
            set => SetField(ref name, value, nameof(Name));
            /*
            set
            {
                if (_name != value)
                {
                    _name = value;
                    //Raise the event
                    OnNotifyPropertyChanged();
                }
            }
            */
        }

        private object SetField(ref string name, string value, string v)
        {
            throw new NotImplementedException();
        }

        //Define an event 
        /*
        protected virtual void OnNotifyPropertyChanged([System.Runtime.CompilerServices.CallerMemberName] String propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }*/

        private string _name;

        public event PropertyChangedEventHandler PropertyChanged;
    }
}

//reference
//observable collection example
//https://www.c-sharpcorner.com/UploadFile/e06010/observablecollection-in-wpf/
//implements INotifyPropertyChanged example
//https://docs.microsoft.com/en-us/dotnet/framework/winforms/how-to-implement-the-inotifypropertychanged-interface
