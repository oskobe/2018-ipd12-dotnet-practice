﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PeopleDBEF
{
    class DatabaseContext : DbContext    {
        public DatabaseContext() : base(@"Data Source = (LocalDB)\MSSQLLocalDB; AttachDbFilename=E:\Jing\IPD12-DotNet\PeopleDBEF\PeopleDBEF.mdf;Integrated Security = True; Connect Timeout = 30")
        {
        }

        public DbSet<Person> People { get; set; }
    }
}
