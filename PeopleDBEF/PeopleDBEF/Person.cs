﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PeopleDBEF
{ 
    class Person
    {
        /*
        [Key]
        public int Id { get; set; }
        [MaxLength(50)]
        [Required]
        public string Name { get; set; }
        public int Age { get; set; }
        public double Height { get; set; }
        */
        private int _id;
        private string _name;
        private int _age;
        private double _height;
        private const string ERROR_MESSAGE_NAME = "Name must be 2-50 characters long.";
        private const string ERROR_MESSAGE_AGE = "Age must be 1-150.";

        [Key]
        public int Id
        {
            get
            {
                return _id;
            }
            set
            {
                _id = value;
            }

        }

        [MaxLength(50)]
        [Required]
        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                if (value.Length < 2 || value.Length > 50)
                {
                    throw new InvalidDataException(ERROR_MESSAGE_NAME);
                }
                _name = value;
            }
        }

        public int Age
        {
            get
            {
                return _age;
            }
            set
            {
                if (value < 1 || value > 150)
                {
                    throw new InvalidDataException(ERROR_MESSAGE_AGE);
                }
                _age = value;
            }
        }

        public double Height
        {
            get
            {
                return _height;
            }
            set
            {
                _height = double.Parse(string.Format("{0:0.0}", value));
            }
        }
        
    }
}

