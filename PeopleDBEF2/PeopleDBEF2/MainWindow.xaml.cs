﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PeopleDBEF2
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        DatabaseContext ctx;
        List<Person> peopleList;
        int idxSelectedPerson = -1;
        Person selectedPerson = null;
        const string CENTIMETER = " CM";

        public MainWindow()
        {
            InitializeComponent();
            ctx = new DatabaseContext();
            LoadPeopleList();
        }

        private void LoadPeopleList()
        {

            peopleList = (from p in ctx.People select p).ToList();
            lvPeople.ItemsSource = peopleList; //db.GetAllPeople();
        }

        private void btAdd_Click(object sender, RoutedEventArgs e)
        {
            string name = tbName.Text;
            string ageStr = tbAge.Text;
            int age;
            double height = sldHeight.Value;

            if (name.Trim().Equals(""))
            {
                return;
            }
            if (!int.TryParse(ageStr.Trim(), out age))
            {
                MessageBox.Show("Age must be integer number.", "Input error",
                    MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            try
            {
                Person person = new Person { Name = name, Age = age, Height = height };
                ctx.People.Add(person);
                ctx.SaveChanges();
                LoadPeopleList();
            }
            catch (InvalidDataException ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Input error",
                    MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

        }

        private void btUpdate_Click(object sender, RoutedEventArgs e)
        {
            string name = tbName.Text;
            string ageStr = tbAge.Text;
            int age;
            Person person = null;
            double height = sldHeight.Value;

            if (lblId.Content.Equals("..."))
            {
                MessageBox.Show("Please choose one person.", "Selection error",
                    MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            if (name.Trim().Equals(""))
            {
                return;
            }
            if (!int.TryParse(ageStr.Trim(), out age))
            {
                MessageBox.Show("Age must be integer number.", "Input error",
                    MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            // Do not use person object, just for valid the input data 
            try
            {
                //person = new Person(selectedPerson.Id, name, age, height);
                /*
                var people = (from p in ctx.People where p.Id == selectedPerson.Id select p).ToList();
                people[0].Name = name;
                people[0].Age = age;
                people[0].Height = height;
                */
                selectedPerson.Name = name;
                selectedPerson.Age = age;
                selectedPerson.Height = height;
                ctx.SaveChanges();
                //db.UpdatePerson(person);
                LoadPeopleList();
            }
            catch (InvalidDataException ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Input error",
                    MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }


        }

        private void lvPeople_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //ListViewItem lvi = (ListViewItem)lvPeople.SelectedItem;
            selectedPerson = (Person)lvPeople.SelectedItem;
            if (selectedPerson != null)
            {
                lblId.Content = selectedPerson.Id;
                tbName.Text = selectedPerson.Name;
                tbAge.Text = selectedPerson.Age.ToString();
                sldHeight.Value = selectedPerson.Height;
                lblHeight.Content = selectedPerson.Height + CENTIMETER;
                idxSelectedPerson = lvPeople.SelectedIndex;
            }
            else
            {
                lblId.Content = "...";
            }

        }

        private void DeletePerson_Click(object sender, RoutedEventArgs e)
        {
            if (lvPeople.SelectedItem == null)
            {
                MessageBox.Show("Please select one person.", "Selection Error", MessageBoxButton.OK,
                    MessageBoxImage.Warning);
                return;
            }

            MessageBoxResult result = MessageBox.Show("Are you sure to delete the person: " + selectedPerson.Id + "#  \"" + selectedPerson.Name + "\".", "Delete confirmataion", MessageBoxButton.YesNo, MessageBoxImage.Question);

            if (result == MessageBoxResult.Yes)
            {

                ctx.People.Remove(selectedPerson);
                ctx.SaveChanges();
                LoadPeopleList();

            }
        }

        private void sldHeight_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (lblHeight != null)
            {
                lblHeight.Content = string.Format("{0:0.0}", sldHeight.Value) + CENTIMETER;
            }

        }

        private void lvPeople_MouseRightButtonUp(object sender, MouseButtonEventArgs e)
        {
            ContextMenu myContextMenu = new ContextMenu(); ;

            MenuItem menuItem = new MenuItem();
            menuItem.Header = "Delete";
            myContextMenu.Items.Add(menuItem);
            menuItem.Click += new RoutedEventHandler(DeletePerson_Click);

            myContextMenu.IsOpen = true;
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            ctx.Dispose();
        }
    }
}
