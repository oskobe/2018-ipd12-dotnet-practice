﻿

using System;
using System.Collections.Generic;

namespace IndexPlay
{
    class String10Storage
    {
        private string[] data = new string[10];

        public string this[int pos]
        {
            get
            {
                return data[pos];
            }
            set
            {
                data[pos] = value;
            }
        }
    }

    class PrimeArray
    {
        public bool this[int pos]
        {
            get
            {
                for (int i = 2; i <= Math.Sqrt(pos); i++)
                {
                    if (pos % i == 0)
                    {
                        return false;
                    }
                }
                return true;
            }
        }    
    }

    class PrimeArray2
    {
        public int this[int pos]
        {
            get
            {
                int number = 1;
                int count = 0;
                while (true)
                {
                    if (checkIfPrimeNumber(number))
                    {
                        count++;
                        if (count == pos)
                        {
                            return number;
                        }
                    }
                    number++;
                }
                
            }
        }

        bool checkIfPrimeNumber(int num)
        {
            for (int i = 2; i <= Math.Sqrt(num); i++)
            {
                if (num % i == 0)
                {
                    return false;
                }
            }
            return true;
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            // Indexer test
            String10Storage s10s = new String10Storage();
            s10s[3] = "Dalian";
            s10s[8] = "Maritime";
            s10s[1] = "University";
            s10s[5] = "It's Perfect";
            s10s[0] = "Hello";

            for (int i = 0; i < 10; i++)
            {
                Console.WriteLine("[{0:00}]: {1}", i, s10s[i]);
            }


            // Prime Number Test
            PrimeArray pa = new PrimeArray();
            for (var i = 1; i <= 30; i++)
            {
                Console.WriteLine("PA[{0:00}] returns {1}", i, pa[i]);
            }

            // Prime Number 2 Test
            PrimeArray2 pa2 = new PrimeArray2();
            for (var i = 1; i <= 30; i++)
            {
                Console.WriteLine("PA[{0:00}] returns {1}", i, pa2[i]);
            }

            Console.ReadLine();
        }
    }
}
