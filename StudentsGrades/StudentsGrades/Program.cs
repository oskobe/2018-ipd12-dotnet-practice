﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace StudentsGrades
{
    class Program
    {
        static Dictionary<string, double> letterGradeToGpaList = new Dictionary<string, double>();

        static void Main(string[] args)
        {
            string path = "../../grades.txt";
            StreamReader sr = new StreamReader(path, Encoding.Default);
            string line;
            string[] lineArr;
            List<double> grades = new List<double>();
            Dictionary<string, double> gradeReport = new Dictionary<string, double>();
            string name;
            //const string HASGPA = " has GPA ";

            // Initialize gpa list
            initializeLetterGradeToGpaList();

            // Process average gpa
            while ((line = sr.ReadLine()) != null)
            {
                lineArr = line.Split(new string[] { ":", "," }, StringSplitOptions.None);
                grades.Clear();
                name = "";
                for (int i = 0; i < lineArr.Count(); i++)
                {
                    if (i == 0)
                    {
                        name = lineArr[0];
                        gradeReport.Add(name, 0.00);
                        continue;
                    }
                    //Console.WriteLine(lineArr[i]);
                    grades.Add(letterToNumberGrade(lineArr[i]));
                }

                gradeReport[name] = grades.Average();
            }

            // Print gpa
            foreach(KeyValuePair<string, double> kvp in gradeReport)
            {
                //Console.WriteLine(kvp.Key + HASGPA + kvp.Value);
                Console.WriteLine("{0} has GPA {1:0.00}", kvp.Key, kvp.Value);
            }
            Console.ReadLine();
        }

        static void initializeLetterGradeToGpaList()
        {
            letterGradeToGpaList.Add("A", 4.00);
            letterGradeToGpaList.Add("A-", 3.67);
            letterGradeToGpaList.Add("B+", 3.33);
            letterGradeToGpaList.Add("B", 3.00);
            letterGradeToGpaList.Add("B-", 2.67);
            letterGradeToGpaList.Add("C+", 2.33);
            letterGradeToGpaList.Add("C", 2.00);
            letterGradeToGpaList.Add("C-", 1.67);
            letterGradeToGpaList.Add("D+", 1.33);
            letterGradeToGpaList.Add("D", 1.00);
            letterGradeToGpaList.Add("D-", 0.67);
            letterGradeToGpaList.Add("F", 0.00);
        }

        static double letterToNumberGrade(string strGrade)
        {
            if (letterGradeToGpaList.ContainsKey(strGrade))
            {
                return letterGradeToGpaList[strGrade];
            }
            return 0;
        }


    }
}
