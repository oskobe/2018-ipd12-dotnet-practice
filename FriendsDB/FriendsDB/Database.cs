﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FriendsDB
{
    class Database
    {
        SqlConnection conn;

        public Database() 
        {
            string dbConnectString = "Data Source=myjac.database.windows.net;Initial Catalog=myJacDB;Persist Security Info=True;User ID=jing;Password=1qaz@WSX";

            conn = new SqlConnection(dbConnectString);
            conn.Open();

        }

        public List<Friend> GetAllFriends ()
        {
            List<Friend> list = new List<Friend>();
            SqlCommand command = new SqlCommand("SELECT * FROM friends", conn);

            using (SqlDataReader reader = command.ExecuteReader())
            {
                while (reader.Read())
                {
                    Friend friend = new Friend();
                    friend.Id = (int)reader[0];
                    friend.Name = (string)reader[1];
                    list.Add(friend);
                }
                return list;
            }
        }

        public void AddFriend(string name)
        {
            using (SqlCommand insertCommand = new SqlCommand("INSERT INTO friends (name) VALUES (@name)", conn))
            {
                insertCommand.Parameters.AddWithValue(@"name", name);
                insertCommand.ExecuteNonQuery();
            }
                
        }
    }
}
