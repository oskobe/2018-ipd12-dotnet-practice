﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FriendsDB
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        Database db = new Database();
        List<Friend> friendList = null;

        public MainWindow()
        {
            InitializeComponent();
            //lvFriend.ItemsSource = friendList;
            LoadFridendList();
        }

        private void LoadFridendList()
        {
            friendList = db.GetAllFriends();
            lvFriend.Items.Clear();

            foreach(Friend f in friendList)
            {
                lvFriend.Items.Add(f);
            }
            //lvFriend.Items.Refresh();
        }

        private void btAdd_Click(object sender, RoutedEventArgs e)
        {
            db.AddFriend(tbName.Text);
            LoadFridendList();
        }
    }
}
