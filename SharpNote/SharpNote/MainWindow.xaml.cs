﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SharpNote
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        string currentFile;
        bool docChangedFlag;
        bool fileOpenedFlag;
        string currentFileName;
        const string SHARPNOTE = " - SharpNote !";
        const string NEW = "New";
        const string MODIFIED = " (Modified)";
        const string NOFILEOPEN = "No file open";

        public MainWindow()
        {
            InitializeComponent();
            InitializeStatus();
        }

        private void miOpen_Click(object sender, RoutedEventArgs e)
        {
             if (docChangedFlag)
             {
                String msg = (currentFile == null) ? "Do you want to save the new file ?" :
                    "Do you want to save the file: \n" + "\"" + currentFile + "\" ?";

                MessageBoxResult result = MessageBox.Show(msg, "Save", MessageBoxButton.YesNoCancel, MessageBoxImage.Question);

                if (result != MessageBoxResult.Cancel)
                {
                    if (fileOpenedFlag)
                    {
                        // Call "Save" module
                        if (WriteToFile())
                        {
                            // Update window title & status bar;
                            UpdateStatusBarAndWindowTitle();
                        }
                        else
                        {
                            return;
                        }
                    }
                    else
                    {
                        // Call "Save as" module 
                        if (GetFileName(out currentFile))
                        {
                            if (WriteToFile())
                            {
                                // Update window title & status bar;
                                UpdateStatusBarAndWindowTitle();
                            }
                            else
                            {
                                return;
                            }
                        }
                        else
                        {
                            return;
                        }
                        
                    }
                    
                }
                else
                {
                    return;
                }
             }
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Text files (*.txt)|*.txt|All files (*.*)|*.*";

            if (openFileDialog.ShowDialog() == true)
            {
                try
                {
                    currentFile = openFileDialog.FileName;
                    tbDocument.Text = File.ReadAllText(currentFile);

                    // Update status
                    UpdateStatusBarAndWindowTitle();

                }
                catch (IOException ex)
                {
                    Console.WriteLine(ex);
                    MessageBox.Show("Error opening file: \n" + ex.Message, "File open error",
                        MessageBoxButton.OK, MessageBoxImage.Error);
                }

            }

        }

        private void tbDocument_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (!docChangedFlag)
            {
                mainWindow.Title = currentFileName + MODIFIED + SHARPNOTE;
                docChangedFlag = true;
            }
        }

        private void mainWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (docChangedFlag)
            {
                String msg = (currentFile == null)? "Do you want to save the new file ?" : 
                    "Do you want to save the file: \n" + "\"" + currentFile + "\" ?";

                MessageBoxResult result = MessageBox.Show(msg, "Save", MessageBoxButton.YesNoCancel, MessageBoxImage.Question);
                switch (result)
                {
                    case MessageBoxResult.Yes:
                        if (fileOpenedFlag)
                        {
                            // Call "Save" module
                            if (WriteToFile())
                            {
                                // Update window title & status bar;
                                UpdateStatusBarAndWindowTitle();
                            }
                        } 
                        else
                        {
                            // Call "Save as" module 
                            if (GetFileName(out currentFile))
                            {
                                if (WriteToFile())
                                {
                                    // Update window title & status bar;
                                    UpdateStatusBarAndWindowTitle();
                                }
                            }
                            
                        }
                        break;
                    case MessageBoxResult.No:
                        break;
                    case MessageBoxResult.Cancel:
                        e.Cancel = true;
                        break;

                }

            }
        }

        private void miNew_Click(object sender, RoutedEventArgs e)
        {
            if (docChangedFlag)
            {
                String msg = (currentFile == null) ? "Do you want to save the new file ?" :
                    "Do you want to save the file: \n" + "\"" + currentFile + "\" ?";

                MessageBoxResult result = MessageBox.Show(msg, "Save", MessageBoxButton.YesNoCancel, MessageBoxImage.Question);
                switch (result)
                {
                    case MessageBoxResult.Yes:
                        if (fileOpenedFlag)
                        {
                            // Call "Save" module
                            if (WriteToFile())
                            {
                                // Update window title & status bar;
                                UpdateStatusBarAndWindowTitle();
                            }
                        }
                        else
                        {
                            // Call "Save as" module 
                            if (GetFileName(out currentFile))
                            {
                                if (WriteToFile())
                                {
                                    // Update window title & status bar;
                                    UpdateStatusBarAndWindowTitle();
                                }
                            }
                            
                        }
                        InitializeStatus();
                        break;
                    case MessageBoxResult.No:
                        InitializeStatus();
                        break;
                    case MessageBoxResult.Cancel:
                        break;

                }

            }

            
        }

        private void miSaveAs_Click(object sender, RoutedEventArgs e)
        {
            if (GetFileName(out currentFile))
            {
                if (WriteToFile())
                {
                    // Update window title & status bar;
                    UpdateStatusBarAndWindowTitle();
                }
            }
  
        }

        private void miSave_Click(object sender, RoutedEventArgs e)
        {
            if (WriteToFile())
            {
                // Update window title & status bar;
                UpdateStatusBarAndWindowTitle();
            }
        }

        private void miExit_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private bool GetFileName(out string file)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "Text files (*.txt)|*.txt|All files (*.*)|*.*";
                        
            if (saveFileDialog.ShowDialog() == true)
            {
                file = saveFileDialog.FileName;
                return true;
            }
            else
            {
                file = currentFile;
                return false;
            }
                
        }

        private bool WriteToFile()
        {
            try
            {
                File.WriteAllText(currentFile, tbDocument.Text);
                return true;
            }
            catch (IOException ex)
            {
                Console.WriteLine(ex);
                MessageBox.Show("Error writing file: \n" + ex.Message, "File write error",
                    MessageBoxButton.OK, MessageBoxImage.Error);
                return false;
            }
        }

        private void InitializeStatus()
        {
            currentFile = null;
            currentFileName = "New";
            tbDocument.Text = "";
            docChangedFlag = false;
            fileOpenedFlag = false;
            tbStatus.Text = NOFILEOPEN;
            mainWindow.Title = currentFileName + SHARPNOTE;
        }

        private void UpdateStatusBarAndWindowTitle()
        {
            docChangedFlag = false;
            fileOpenedFlag = true;
            tbStatus.Text = currentFile;
            currentFileName = currentFile.Substring(currentFile.LastIndexOf("\\") + 1);
            mainWindow.Title = currentFileName + SHARPNOTE;
        }
    }
}
