﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RefOutTest
{
    class Program
    {
        static void Main(string[] args)
        {
            int myInt = 100;
            Person myPerson = new Person();
            myPerson.Id = 1000;
            myPerson.Name = "Jing Wang";

            ChangeMe(ref myInt, ref myPerson);

            Console.WriteLine(myInt + "");
            Console.WriteLine(myPerson.Id + " " + myPerson.Name);
            Console.ReadLine();
        }

        private static void ChangeMe(ref int num, ref Person p)
        {
            num = 200;
            Person p2 = new Person();
            p2.Id = 200;
            p2.Name = "Hello my Name";
            p = p2;
        }
    }
}
