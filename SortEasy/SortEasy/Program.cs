﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SortEasy
{
    class Person
    {
        public string name;
        public int age;

    }

    class Program
    {
        static void Main(string[] args)
        {
            List<Person> list = new List<Person>();
            // we are using "Object Initializer"
            list.Add(new Person() { name = "Jerry", age = 37 });
            list.Add(new Person() { name = "Adam", age = 17 });
            list.Add(new Person() { name = "Jerry", age = 34 });
            list.Add(new Person() { name = "Elvire", age = 65 });

            foreach(Person p in list)
            {
                Console.WriteLine("{0} is {1} years old", p.name, p.age);
            }

            Console.WriteLine("---- by name ----");
            var listByName = from p in list orderby p.name, p.age select p;

            foreach (Person p in listByName)
            {
                Console.WriteLine("{0} is {1} years old", p.name, p.age);
            }

            Console.ReadLine();



        }
    }
}
