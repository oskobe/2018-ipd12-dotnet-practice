﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MyCustomerControl
{
    /// <summary>
    /// Follow steps 1a or 1b and then 2 to use this custom control in a XAML file.
    ///
    /// Step 1a) Using this custom control in a XAML file that exists in the current project.
    /// Add this XmlNamespace attribute to the root element of the markup file where it is 
    /// to be used:
    ///
    ///     xmlns:MyNamespace="clr-namespace:MyCustomerControl"
    ///
    ///
    /// Step 1b) Using this custom control in a XAML file that exists in a different project.
    /// Add this XmlNamespace attribute to the root element of the markup file where it is 
    /// to be used:
    ///
    ///     xmlns:MyNamespace="clr-namespace:MyCustomerControl;assembly=MyCustomerControl"
    ///
    /// You will also need to add a project reference from the project where the XAML file lives
    /// to this project and Rebuild to avoid compilation errors:
    ///
    ///     Right click on the target project in the Solution Explorer and
    ///     "Add Reference"->"Projects"->[Select this project]
    ///
    ///
    /// Step 2)
    /// Go ahead and use your control in the XAML file.
    ///
    ///     <MyNamespace:CustomControl1/>
    ///
    /// </summary>
    public class MyTextBox : TextBox
    {
        static DependencyProperty WatermarkProperty;
        
        public MyTextBox()
        {
           
            WatermarkProperty = DependencyProperty.Register("Watermark", typeof(string), typeof(MyTextBox));

            Loaded += MyTextBox_Loaded;
            GotFocus += MyTextBox_GotFocus;
            LostFocus += MyTextBox_LostFocus;
            TextChanged += MyTextBox_TextChanged;
        }

        void MyTextBox_Loaded(object sender, EventArgs e)
        {
            this.Text = Watermark;
            this.Foreground = Brushes.DarkGray;
        }

        void MyTextBox_GotFocus(object sender, EventArgs e)
        {
            if (this.Text.Equals(Watermark))
            {
                this.Text = string.Empty;
                this.Foreground = Brushes.Black;
            }
            
        }

        void MyTextBox_LostFocus(object sender, EventArgs e)
        {
            if (this.Text.Trim().Equals(""))
            {
                this.Text = Watermark;
                this.Foreground = Brushes.DarkGray;
            }
        }

        void MyTextBox_TextChanged(object sender, EventArgs e)
        {
            /*
            if (this.Text.Trim().Equals(""))
            {
                this.Text = Watermark;
                this.Foreground = Brushes.DarkGray;
            }
            */
        }

        public string Watermark
        {
            get
            {
                return (string)base.GetValue(WatermarkProperty);
            }

            set
            {
                base.SetValue(WatermarkProperty, value);
            }
        }



    }
}
