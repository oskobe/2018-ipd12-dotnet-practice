﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ScoopSelection
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        //List<string> selectedList = new List<string>();
        ObservableCollection<string> list = new ObservableCollection<string>();

        public MainWindow()
        {
            InitializeComponent();
            //lvSelected.ItemsSource = selectedList;
            lvSelected.ItemsSource = list;
        }

        private void btAdd_Click(object sender, RoutedEventArgs e)
        {
  
            if (lvFlavours.SelectedItem == null)
            {
                MessageBox.Show("Please select one flavour.", "No selection", MessageBoxButton.OK, MessageBoxImage.Warning);
            } else
            {
                string selectedFlavour = ((ListViewItem)lvFlavours.SelectedItem).Content.ToString();
                //string selectedFlavour = (string)lvFlavours.SelectedItem;
                //selectedList.Add(selectedFlavour);
                list.Add(selectedFlavour);
                //lvSelected.Items.Refresh();
            }
 
        }

        private void btClearAll_Click(object sender, RoutedEventArgs e)
        {
            list.Clear();
            //lvSelected.Items.Refresh();
        }

        private void btDelete_Click(object sender, RoutedEventArgs e)
        {
            if (lvSelected.SelectedItem == null)
            {
                MessageBox.Show("Please select one scoop.", "No selection", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            else
            {
                int idx = lvSelected.SelectedIndex;
                //selectedList.Add(selectedFlavour);
                list.RemoveAt(idx);
                //lvSelected.Items.Refresh();
            }
        }
    }
}
