﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MSDNSample
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var context = new BlogContext())
            {
                var b = new Blog { Name = "ttt" };
                context.Blogs.Add(b);
                
                /*
                // Add a new User by setting a reference from a tracked Blog 
                var blog = context.Blogs.Find(3);
                blog.Name = "Changed name3";

                // Add a new Post by adding to the collection of a tracked Blog 
                blog = context.Blogs.Find(3);
                blog.Posts.Add(new Post { Title = "How to Add Entities", BlogId = 2 });
                */
            

                context.SaveChanges();
            }
        }
    }
}
