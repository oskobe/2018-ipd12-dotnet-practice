﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;
using Microsoft.Win32;

namespace ITextSharpWPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void BtCreate_Click(object sender, RoutedEventArgs e)
        {
            Stream myStream;            //文件流  
            SaveFileDialog savefile = new SaveFileDialog();

            savefile.Filter = "pdf files (*.pdf)|*.pdf|All files (*.*)|*.*";    //保存文件的格式  
            savefile.FilterIndex = 1;                   //默认保存文件格式索引，默认为第一种pdf格式  
            savefile.RestoreDirectory = true;               //记忆上次打开目录  

            if (savefile.ShowDialog() == DialogResult.OK)  //点保存之后  
            {
                string localFilePath = savefile.FileName.ToString(); //获得保存文件路径   
                string fileNameExt = localFilePath.Substring(localFilePath.LastIndexOf("\\") + 1); //获取文件名，不带路径  

                myStream = savefile.OpenFile();         //打开文件并赋给IO流myStream  

                Document document = new Document(PageSize.A4.Rotate());         //创建A4纸、横向PDF文档  
                PdfWriter writer = PdfWriter.GetInstance(document, myStream);    //将PDF文档写入创建的文件中  
                document.Open();
                //要在PDF文档中写入中文必须指定中文字体，否则无法写入中文  
                BaseFont bftitle = BaseFont.CreateFont("C:\\Windows\\Fonts\\SIMHEI.TTF", BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED);   //用系统中的字体文件SimHei.ttf创建文件字体  
                iTextSharp.text.Font fonttitle = new iTextSharp.text.Font(bftitle, 30);     //标题字体，大小30  
                BaseFont bf1 = BaseFont.CreateFont("C:\\Windows\\Fonts\\SIMSUN.TTC,1", BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED);     //用系统中的字体文件SimSun.ttc创建文件字体  
                iTextSharp.text.Font CellFont = new iTextSharp.text.Font(bf1, 12);          //单元格中的字体，大小12  
                iTextSharp.text.Font fonttitle2 = new iTextSharp.text.Font(bf1, 15);        //副标题字体，大小15  

                //添加标题  
                Paragraph Title = new Paragraph("示例文件", fonttitle);     //添加段落，第二个参数指定使用fonttitle格式的字体，写入中文必须指定字体否则无法显示中文  
                Title.Alignment = iTextSharp.text.Rectangle.ALIGN_CENTER;       //设置居中  
                document.Add(Title);        //将标题段加入PDF文档中  

                //空一行  
                Paragraph nullp = new Paragraph(" ", fonttitle2);
                nullp.Leading = 10;
                document.Add(nullp);

                PdfPTable table = new PdfPTable(int)numericUpDown2.Value);         //numericUpDown2为用户设置的列数，创建Value列的表格,行会根据写入数据自动扩展  

                //不同单元格对应tablelayoutpanel添加不同文本  
                for (int j = 0; j < tableLayoutPanel1.RowCount; j++)//j为行标   
                {
                    for (int i = 0; i < tableLayoutPanel1.ColumnCount; i++)//i为列表   
                    {
                        if (i == 0 && j == 0) //左上角为空   
                        {
                            table.AddCell(" ");//向表格的单元格添加数据，此处为空白   
                            continue;
                        }
                        if (j == 0) //第一行标号   
                        {
                            table.AddCell(i + "#");
                            continue;
                        }
                        if (i == 0 && j > 0) //tablelayoutpanel第一列为textbox控件，读取用户输入的文本   
                        {
                            Control c = tableLayoutPanel1.GetControlFromPosition(i, j);//获取tablelayoutpannel容器中第i列、第j行的控件   
                            if (c is TextBox)//判定控件的类型如果为textbox则将文本内容写入PDF文档   
                            {
                                table.AddCell(new Paragraph(c.Text, CellFont));//用CellFont字体将textbox中的内容写入PDF文档的单元格中   
                            }
                            continue;
                        }
                        if (i > 0 && j > 0) //单元格数据   
                        {
                            Control c = tableLayoutPanel1.GetControlFromPosition(i, j);
                            if (c is Label)
                            {
                                table.AddCell(c.Text);
                            }
                            else
                            {
                                table.AddCell(" ");
                            }
                            continue;
                        }

                        table.AddCell(" "); //如果tablelayoutpanel单元格中不存在控件则写入空单元格   
                    }
                }

                document.Add(table); //将表格加入PDF文档中   
                document.Close(); myStream.Close();
            }
        }
    }
}
