﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;
using Microsoft.Win32;


namespace ITtextSharpDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            Document doc = new Document(PageSize.LETTER);
            PdfPTable table = new PdfPTable(3);

            Stream myStream;            //文件流  
            SaveFileDialog savefile = new SaveFileDialog();

            savefile.Filter = "pdf files (*.pdf)|*.pdf|All files (*.*)|*.*";    //保存文件的格式  
            savefile.FilterIndex = 1;                   //默认保存文件格式索引，默认为第一种pdf格式  
            savefile.RestoreDirectory = true;               //记忆上次打开目录  

            if (savefile.ShowDialog() == DialogResult.OK)  //点保存之后  
            {
                string localFilePath = savefile.FileName.ToString(); //获得保存文件路径   
                string fileNameExt = localFilePath.Substring(localFilePath.LastIndexOf("\\") + 1); //获取文件名，不带路径  

                myStream = savefile.OpenFile();
            }

            PdfPCell cell = new PdfPCell(new Phrase("Header spanning 3 columns"));



            cell.Colspan = 3;

            cell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right  

            table.AddCell(cell);

            table.AddCell("Col 1 Row 1");

            table.AddCell("Col 2 Row 1");

            table.AddCell("Col 3 Row 1");

            table.AddCell("Col 1 Row 2");

            table.AddCell("Col 2 Row 2");

            table.AddCell("Col 3 Row 2");

            PdfWriter writer = PdfWriter.GetInstance(doc, myStream);    //将PDF文档写入创建的文件中  
            doc.Open();
            doc.Open();

            doc.Add(table);


            Console.ReadLine();

        }
    }
}
