namespace TEST3.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class xxxx : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Posts", "BlogId", "dbo.Blogs");
            DropIndex("dbo.Posts", new[] { "BlogId" });
            RenameColumn(table: "dbo.Posts", name: "BlogId", newName: "Blog_Id");
            AddColumn("dbo.Posts", "BBId", c => c.Int(nullable: false));
            AlterColumn("dbo.Posts", "Blog_Id", c => c.Int());
            CreateIndex("dbo.Posts", "Blog_Id");
            AddForeignKey("dbo.Posts", "Blog_Id", "dbo.Blogs", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Posts", "Blog_Id", "dbo.Blogs");
            DropIndex("dbo.Posts", new[] { "Blog_Id" });
            AlterColumn("dbo.Posts", "Blog_Id", c => c.Int(nullable: false));
            DropColumn("dbo.Posts", "BBId");
            RenameColumn(table: "dbo.Posts", name: "Blog_Id", newName: "BlogId");
            CreateIndex("dbo.Posts", "BlogId");
            AddForeignKey("dbo.Posts", "BlogId", "dbo.Blogs", "Id", cascadeDelete: true);
        }
    }
}
