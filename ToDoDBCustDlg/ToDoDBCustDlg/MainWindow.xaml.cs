﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ToDoDBCustDlg
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            try
            {
                Globals.db = new Database();
                InitializeComponent();
                refreshTodoList();

            }
            catch (SqlException e)
            {
                Console.WriteLine(e.StackTrace);
                MessageBox.Show("Error opening database connection: " + e.Message);
                Environment.Exit(1);
            }

        }


        private void refreshTodoList()
        {
            lvTodos.ItemsSource = Globals.db.GetAllTodos();
        }

        private void btAddMain_Click(object sender, RoutedEventArgs e)
        {
            AddEditTodoDialog dlg = new AddEditTodoDialog(null);
            if (dlg.ShowDialog() == true)
            {
                lvTodos.ItemsSource = Globals.db.GetAllTodos();
            }

        }

        private void lvTodos_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            Todo todo = (Todo)lvTodos.SelectedItem;
            /*
            if (todo == null)
            {
                return;
            }
            */
            // USE FOLLOWING markup to make sure event only occurs when the item was seleted
            /*
            < ListView.ItemContainerStyle >
                < Style TargetType = "ListViewItem" >
                     < EventSetter Event = "MouseRightButtonUp" Handler = "lvTodos_MouseRightButtonUp" />
    
                     < EventSetter Event = "MouseDoubleClick" Handler = "lvTodos_MouseDoubleClick" />      
                </ Style >
            </ ListView.ItemContainerStyle > 
            */


            AddEditTodoDialog dlg = new AddEditTodoDialog(todo);
            if (dlg.ShowDialog() == true)
            {
                lvTodos.ItemsSource = Globals.db.GetAllTodos();
            }
        }

        private void tbSearch_TextChanged(object sender, TextChangedEventArgs e)
        {
            // TODO: don't re-fetch from date every time , use a cached version instead
            List<Todo> todoList = Globals.db.GetAllTodos();

            string word = tbSearch.Text;
            if (word != "")
            {
                var result = from t in todoList where t.Task.Contains(word) select t;
                todoList = result.ToList();
            }

            lvTodos.ItemsSource = todoList;
        }

        private void lvTodos_MouseRightButtonUp(object sender, MouseButtonEventArgs e)
        {
            ContextMenu myContextMenu = new ContextMenu();

            MenuItem menuItem = new MenuItem();
            menuItem.Header = "Delete";
            myContextMenu.Items.Add(menuItem);
            menuItem.Click += new RoutedEventHandler(DeleteTodo_Click);

            myContextMenu.IsOpen = true;
        }

        private void DeleteTodo_Click(object sender, RoutedEventArgs e)
        {
            Todo selectedTodo = (Todo)lvTodos.SelectedItem;
            if (lvTodos.SelectedItem == null)
            {
                MessageBox.Show("Please select one person.", "Selection Error", MessageBoxButton.OK,
                    MessageBoxImage.Warning);
                return;
            }

            MessageBoxResult result = MessageBox.Show("Are you sure to delete the person: " + selectedTodo.Id + "#  \"" + selectedTodo.Task + "\".", "Delete confirmataion", MessageBoxButton.YesNo, MessageBoxImage.Question);

            if (result == MessageBoxResult.Yes)
            {
                Globals.db.DeleteTodo(selectedTodo.Id);
                refreshTodoList();
            }
        }
    }



}
