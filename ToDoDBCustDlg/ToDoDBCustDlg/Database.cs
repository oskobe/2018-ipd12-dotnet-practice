﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ToDoDBCustDlg
{
    class Database
    {
        SqlConnection conn;

        public Database()
        {
            string dbConnectString = "Data Source=myjac.database.windows.net;Initial Catalog=myJacDB;Persist Security Info=True;User ID=jing;Password=1qaz@WSX";

            conn = new SqlConnection(dbConnectString);
            conn.Open();
        }

        public List<Todo> GetAllTodos()
        {
            List<Todo> list = new List<Todo>();
            SqlCommand command = new SqlCommand("SELECT * FROM todo", conn);

            using (SqlDataReader reader = command.ExecuteReader())
            {
                while (reader.Read())
                {
                    Todo Todo = new Todo() { Id = (int)reader[0], Task = (string)reader[1], DueDate = (DateTime)reader[2], IsDone = (byte)reader[3] != 0  };
                    list.Add(Todo);
                }
                return list;
            }

        }

        public void AddTodo(Todo Todo)
        {
            using (SqlCommand insertCommand = new SqlCommand("INSERT INTO todo (task, dueDate, isDone) VALUES (@task, @dueDate, @isDone)", conn))
            {
                insertCommand.Parameters.AddWithValue("@task", Todo.Task);
                insertCommand.Parameters.AddWithValue("@dueDate", Todo.DueDate);
                // db will transfer the bool to 1 or 0 automatically, so that giving the bool to tiny int field directly is okay 
                insertCommand.Parameters.AddWithValue("@isDone", Todo.IsDone == true ? 1 : 0);
                insertCommand.ExecuteNonQuery();
            }

        }

        public void UpdateTodo(Todo Todo)
        {
            using (SqlCommand insertCommand = new SqlCommand("UPDATE todo SET task = @task, dueDate = @dueDate, isDone = @isDone WHERE id = @id", conn))
            {
                insertCommand.Parameters.AddWithValue(@"task", Todo.Task);
                insertCommand.Parameters.AddWithValue(@"dueDate", Todo.DueDate);
                insertCommand.Parameters.AddWithValue(@"isDone", Todo.IsDone == true ? 1 : 0);
                insertCommand.Parameters.AddWithValue(@"id", Todo.Id);
                insertCommand.ExecuteNonQuery();
            }

        }

        public void DeleteTodo(int id)
        {
            using (SqlCommand insertCommand = new SqlCommand("DELETE FROM todo WHERE id = @id", conn))
            {
                insertCommand.Parameters.AddWithValue(@"id", id);
                insertCommand.ExecuteNonQuery();
            }

        }
    }
}
