﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ToDoDBCustDlg
{
    /// <summary>
    /// Interaction logic for AddEditTodoDialog.xaml
    /// </summary>
    public partial class AddEditTodoDialog : Window
    {
        private Todo currentItem;

        public AddEditTodoDialog(Todo item)
        {
            currentItem = item;

            InitializeComponent();
            
            if (currentItem == null)
            {
                btSave.Content = "Add new";
            } else
            {
                lblId.Content = item.Id + "";
                tbTask.Text = item.Task;
                dpDueDate.SelectedDate = item.DueDate;
                chkbIsDone.IsChecked = item.IsDone;
            }

            
        }

        private void btSave_Click(object sender, RoutedEventArgs e)
        {
            Todo todo = currentItem == null ? new Todo() : currentItem;

            if (tbTask.Text.Trim().Equals(""))
            {
                return;
            }
            todo.Task = tbTask.Text;

            // Fixme: check if date is null, if none was selected
            if (dpDueDate.SelectedDate != null)
            {
                todo.DueDate = (DateTime)dpDueDate.SelectedDate;
            }
            else
            {
                MessageBox.Show("Please choose a date for the due date", "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            
            todo.IsDone = chkbIsDone.IsChecked ?? false;

            if (currentItem == null)
            // add new
            {
                Globals.db.AddTodo(todo);
            }
            else
            // update old
            {
                Globals.db.UpdateTodo(todo);
            }

            DialogResult = true;
        }

        private void btCancle_Click(object sender, RoutedEventArgs e)
        {
            // By setting isCancel = true, don't need to set false => dialogResult
            //DialogResult = false;
        }
    }
}
