﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DelegateTest4
{

    class Program
    {
        public delegate void Del(string message);

        public static void MethodWithCallback(int param1, int param2, Del callback)
        {
            callback("The number is: " + (param1 + param2).ToString());
        }

        public static void DelegateMethod(string message)
        {
            Console.WriteLine(message);
        }

        static void Main(string[] args)
        {

            Del handler = DelegateMethod;
            MethodWithCallback(1, 2, handler);

            List<int> list = getPrimeNumbers(10);
            int counter = 0;
            foreach (int i in list)
            {
                counter++;
                Console.WriteLine("PA[{0:00}] prime number is {1}", counter, i);
            }

            Console.ReadLine();
        }


        public static List<int> getPrimeNumbers(int numOfPrimeWanted)
        {
            List<int> list = new List<int>();
            list.Add(2);
            int currentNumber = 2;
            bool notPrime;
            int primeFound = 1;

            while (primeFound < numOfPrimeWanted)
            {
                notPrime = false;
                currentNumber++;

                foreach (int i in list)
                {
                    if (i > Math.Sqrt(currentNumber)) {
                        break;
                    }
                    if (currentNumber % i == 0)
                    {
                        notPrime = true;
                        break;
                    }
                }
                if (!notPrime)
                {
                    list.Add(currentNumber);
                    primeFound++;
                }
            }

            return list;
            
        }
    }
}
