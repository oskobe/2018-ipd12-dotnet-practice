﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Multiple_Windows
{
    /// <summary>
    /// Interaction logic for Window2.xaml
    /// </summary>
    public partial class Window2 : Window
    {
        private int _status;

        public Window2()
        {
            InitializeComponent();
        }

        private void BtGoWin3_Click(object sender, RoutedEventArgs e)
        {
            Window3 win3 = new Window3();
            this.Hide();
            win3.ShowDialog();

            if (win3.Status == 0)
            {
                this.ShowDialog();
            }
            else if (win3.Status == 9)
            {
                _status = 9;
                this.Close();
            }
            /*
            if (win3.DialogResult == false)
            {
                this.ShowDialog();
            } else
            {
                this.DialogResult = true;
            }
            */
        }

        private void BtCancel_Click(object sender, RoutedEventArgs e)
        {
            //this.DialogResult = false;
            _status = 0;
            this.Close();
        }

        public int Status
        {
            get
            {
                return _status;
            }
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (_status != 9)
            {
                _status = 0;
            }
        }
    }
}
