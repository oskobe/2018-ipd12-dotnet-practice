﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Multiple_Windows
{
    /// <summary>
    /// Interaction logic for Window3.xaml
    /// </summary>
    public partial class Window3 : Window
    {
        private int _status;
        public Window3()
        {
            InitializeComponent();
        }

        private void Confirm_Click(object sender, RoutedEventArgs e)
        {
            //this.DialogResult = true;
            _status = 9;
            this.Close();
            //this.DialogResult = true;
        }

        public int Status
        {
            get
            {
                return _status;
            }
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            /*
            if (this.DialogResult == true)
            {
                _status = 9;
            } else
            {
                _status = 0;
            }
            */
            if (_status != 9)
            {
                _status = 0;
            }
                
            
            
        }

        private void BtCancel_Click(object sender, RoutedEventArgs e)
        {
            //this.DialogResult = false;
            _status = 0;
            this.Close();
        }
    }
}
