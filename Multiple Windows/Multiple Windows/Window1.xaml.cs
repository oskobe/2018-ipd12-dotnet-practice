﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Multiple_Windows
{
    /// <summary>
    /// Interaction logic for Window1.xaml
    /// </summary>
    public partial class Window1 : Window
    {
        public Window1()
        {
            InitializeComponent();
        }

        private void BtGoWin2_Click(object sender, RoutedEventArgs e)
        {
            
            Window2 win2 = new Window2();
            this.Hide();
            win2.ShowDialog();

            if (win2.Status == 0)
            {
                this.ShowDialog();
            }
            else if (win2.Status == 9)
            {
  
                this.Close();
            }

            /*

            if (win2.DialogResult == false)
            {
                this.ShowDialog();
            } else
            {
                this.DialogResult = true;
            }

            */
            


        }

        private void BtCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
