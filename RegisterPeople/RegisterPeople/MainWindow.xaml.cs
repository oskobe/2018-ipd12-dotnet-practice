﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace RegisterPeople
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void btRegister_Click(object sender, RoutedEventArgs e)
        {
            // Check name
            string name = tbName.Text;
            int nameLen = name.Length;
            if (nameLen < 2 || nameLen > 50 )
            {
                MessageBox.Show("The name must be 2-50 characters.", "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (name.Contains(";"))
            {
                MessageBox.Show("The name can not have semicolon (;).", "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            // Check age
            int age;
            if (!int.TryParse(tbAge.Text, out age))
            {
                MessageBox.Show("The age must be a integer number.", "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (age<1 || age > 150)
            {
                MessageBox.Show("The age must be 1-150.", "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }


            // Gender
            string gender = "";
            if ((bool)rbMale.IsChecked)
            {
                gender = "M";
            }

            if ((bool)rbFemale.IsChecked)
            {
                gender = "F";
            }

            if ((bool)rbNA.IsChecked)
            {
                gender = "N/A";
            }

            // Pets
            List<string> petList = new List<string>();
            if (chkbCat.IsChecked??false)
            {
                petList.Add("Cat(s)");
            }

            if (chkbDog.IsChecked??false)
            {
                petList.Add("Dog(s)");
            }

            if (chkbOther.IsChecked??false)
            {
                petList.Add("Other");
            }

            string pets = "";
            if (petList.Count > 0)
            {
                pets = String.Join(",", petList);
            }
                

            // Continent
            string continent = cbContinent.Text;

            // Create a list for save all person info
            List<string> personInfoList = new List<string>();
            personInfoList.Add(name);
            personInfoList.Add(age.ToString());
            personInfoList.Add(gender);
            personInfoList.Add(pets);
            personInfoList.Add(continent);

            string outFileStr = String.Join(";", personInfoList);
            outFileStr += "\n";

            try
            {
                File.AppendAllText("../../outfile.txt", outFileStr);
                MessageBox.Show("File export successfully.", "Success", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            catch (IOException ex)
            {
                MessageBox.Show("File export error.", "Output error", MessageBoxButton.OK, MessageBoxImage.Error);

            }
        }

    }
}
