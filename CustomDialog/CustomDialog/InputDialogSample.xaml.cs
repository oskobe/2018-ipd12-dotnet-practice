﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace CustomDialog
{
    /// <summary>
    /// Interaction logic for InputDialogSample.xaml
    /// </summary>
    public partial class InputDialogSample : Window
    {
        public InputDialogSample(string question, string defaultAnswer = "")
        {
            InitializeComponent();
            lblQuestion.Content = question;
            txtAnswer.Text = defaultAnswer;
        }

        private void btnDialogOk_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;

            Dictionary<string, string> myDic = new Dictionary<string, string>();
            myDic.Add("1", "AAA");
            myDic.Add("2", "BBB");
            myDic.Add("3", "CCC");

            myDic["9"] =  "EEE";
            //MessageBox.Show(myDic["19"]);
            MessageBox.Show( myDic.ContainsKey("90").ToString());
            
            //myDic[null] = "XXX";
            foreach (KeyValuePair<string, string> kvp in myDic)
            {
                MessageBox.Show(kvp.Key + " " + kvp.Value + myDic["2"]);
                
            }
        }

        private void Window_ContentRendered(object sender, EventArgs e)
        {
            txtAnswer.SelectAll();
            txtAnswer.Focus();
        }

        public string Answer
        {
            get { return txtAnswer.Text; }
        }
    }
}
