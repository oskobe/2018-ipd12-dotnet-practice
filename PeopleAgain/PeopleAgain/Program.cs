﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace PeopleAgain
{
    class Person
    {
        private string _name = string.Empty;
        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                string pat = @"^[A-Za-z\.\-\s]{1,20}$";
                Regex regex = new Regex(pat);
                Match match = regex.Match(value);
                if (match.Success)
                {
                    _name = value;
                }
                else
                {
                    throw new InvalidDataException("Name must be 1-20 characters long, only lowercase, uppercase letters, dots, dash '-' and spaces are allowed");
                }
            }
        }

        private int _age = -1;
        public int Age
        {
            get
            {
                return _age;
            }
            set
            {
                if (value >= 1 && value <= 150)
                {
                    _age = value;
                }
                else
                {
                    throw new InvalidDataException("Age must be between 1 and 150");
                }
                
            }
        }

        public Person(string name, int age)
        {
            Name = name;
            Age = age;
        }

        public override string ToString()
        {
            return (Name + "  " + Age + "  ");
        }
        
    }

    class Teacher : Person
    {
        private string _subject;
        public string Subject
        {
            get
            {
                return _subject;
            }
            set
            {
               
                if (value.Length >= 1 && value.Length <= 20)
                {
                    _subject = value;
                }
                else
                {
                    throw new InvalidDataException("Subject must be 1-20 characters long");
                }
 
            }
        }

        private int _yearsOfExperience;

        public int YearsOfExperience
        {
            get
            {
                return _yearsOfExperience;
            }
            set
            {
                if (value >= 0 && value <= 100)
                {
                    _yearsOfExperience = value;
                }
                else
                {
                    throw new InvalidDataException("Years of experience must be between 0 and 100");
                }
                
            }
        }

        public Teacher(string name, int age, string subject, int yearsOfExperience) : base(name, age)
        {
            Subject = subject;
            YearsOfExperience = yearsOfExperience;
        }

        public override string ToString()
        {
            return base.ToString() + Subject + "  " + YearsOfExperience + " (Teacher)";
        }

    }

    class Student : Person
    {
        private double _gpa;
        public double Gpa
        {
            get
            {
                return _gpa;
            }
            set
            {
                if (value >= 0 && value <= 4.3)
                {
                    _gpa = value;
                }
                else
                {
                    throw new InvalidDataException("GPA must be between 0 and 4.3");
                }
            }
        }

        private string _program;
        public string Program
        {
            get
            {
                return _program;
            }
            set
            {
                if (value.Length >= 1 && value.Length <= 20)
                {
                    _program = value;
                }
                else
                {
                    throw new InvalidDataException("Program must be 1-20 characters long");
                }
            }
        }

        public Student(string name, int age, double gpa, string program) : base(name, age)
        {
            Gpa = gpa;
            Program = program;
        }

        public override string ToString()
        {
            return base.ToString() + Gpa + "  " + Program + " (Student)";
        }

    }

    class Program
    {
        static List<Person> peopleList = new List<Person>();
        const string STUDENT = "Student";
        const string TEACHER = "Teacher";
        const string PERSON = "Person";

        static void Main(string[] args)
        {
            try
            {
                string[] lines = File.ReadAllLines(@"..\..\people.txt");
                
                int idx = 0;
                foreach (string line in lines)
                {
                    idx++;
                    string[] splitByColon = line.Split(':');
                    string type = splitByColon[0];
                    if (!CheckType(type))
                    {
                        Console.WriteLine("Line {0}: the person type [{1}] is wrong", idx, type);
                        continue;
                    }

                    if (splitByColon.Length == 1)
                    {
                        Console.WriteLine("Line {0}: you did not input the person information", idx);
                        continue;
                    }

                    string[] infoArr = splitByColon[1].Split(',');
                    int infoArrLength = infoArr.Length;
                    if (type.Equals(PERSON, StringComparison.OrdinalIgnoreCase)) 
                    {
                        if (infoArrLength < 2)
                        {
                            Console.WriteLine("Line {0}: you did not input all information for a Person", idx);
                            continue;
                        }
                    }
                    else
                    {
                        if (infoArrLength < 4)
                        {
                            Console.WriteLine("Line {0}: you did not input all information for a Teacher or Student", idx);
                            continue;
                        }
                    }

                    string name = infoArr[0];
                    int age = 0;

                    try
                    {
                        age = int.Parse(infoArr[1]);
                    }
                    catch (FormatException ex)
                    {
                        Console.WriteLine("Line {0}: age parse error: {1}", idx, ex.Message);
                        continue;
                    }

                    if (type.Equals(PERSON, StringComparison.OrdinalIgnoreCase))
                    {

                        try
                        {
                            Person person = new Person(name, age);
                            peopleList.Add(person);
                        }
                        catch (InvalidDataException ex)
                        {
                            Console.WriteLine("Line {0}: error: {1}", idx, ex.Message);
                            continue;
                        }

                    }

                    if (type.Equals(TEACHER, StringComparison.OrdinalIgnoreCase))
                    {
                        string subject = infoArr[2];
                        int yearsOfExperience = 0;
                        
                        try 
                        {
                            yearsOfExperience = int.Parse(infoArr[3]);
                            try
                            {
                                Teacher teacher = new Teacher(name, age, subject, yearsOfExperience);
                                peopleList.Add(teacher);
                            } 
                            catch (InvalidDataException ex)
                            {
                                Console.WriteLine("Line {0}: error: {1}", idx, ex.Message);
                                continue;
                            }
                        }
                        catch (FormatException ex)
                        {
                            Console.WriteLine("Line {0}: years of experience parse error: {1}", idx, ex.Message);
                            continue;
                        }

                    }

                    if (type.Equals(STUDENT, StringComparison.OrdinalIgnoreCase))
                    {
                        double gpa = 0;
                        string program = infoArr[3];

                        try
                        {
                            gpa = double.Parse(infoArr[2]);
                            try
                            {
                                Student student = new Student(name, age, gpa, program);
                                peopleList.Add(student);
                            }
                            catch (InvalidDataException ex)
                            {
                                Console.WriteLine("Line {0}: error: {1}", idx, ex.Message);
                                continue;
                            }
                        }
                        catch (FormatException ex)
                        {
                            Console.WriteLine("Line {0}: GPA parse error: {1}", idx, ex.Message);
                            continue;
                        }

                    }
                }
            }
            catch (IOException ex)
            {
                Console.WriteLine("File reading error: " + ex.Message);
            }

            // Print result
            Console.WriteLine();
            Console.WriteLine("====== Before sort ======");
            printPerson(peopleList);

            // Sort
            List<Person> sortedPeopleList = peopleList.OrderBy(o => o.Name).ToList();

            // Print after sort
            Console.WriteLine();
            Console.WriteLine("====== After  sort ======");
            printPerson(sortedPeopleList);

            Console.ReadLine();
        }

        static bool CheckType(string type)
        {
            if (type.Equals(STUDENT, StringComparison.OrdinalIgnoreCase) ||
                type.Equals(TEACHER, StringComparison.OrdinalIgnoreCase) ||
                type.Equals(PERSON, StringComparison.OrdinalIgnoreCase))
            {
                return true;
            }
            return false;
        }

        static void printPerson(List<Person> list)
        {
            foreach (Person p in list)
            {
                Console.WriteLine(p.ToString());
            }
        }
    }
}
