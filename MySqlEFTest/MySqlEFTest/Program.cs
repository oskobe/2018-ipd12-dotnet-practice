﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MySqlEFTest
{
    class Program
    {
        static void Main(string[] args)
        {
            DatabaseContext ctx = new DatabaseContext();
            Person p = new Person { Name = "Lee", Age = 12 };
            ctx.People.Add(p);

            foreach (Person person in ctx.People)
            {
                Console.WriteLine(person.Id + " " + person.Name + " " + person.Age);
            }

            Console.ReadLine();
        }
    }



    class Person
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Age { get; set; }
    }

    class DatabaseContext : DbContext
    {
        public DatabaseContext() : base("name=MySqlDatabaseContext") { }
        public DbSet<Person> People { get; set; }
    }
}
