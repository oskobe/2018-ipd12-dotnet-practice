﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ConnectToDb
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            ConnectToDb();
        }

        private void ConnectToDb()
        {
            using (SqlConnection conn = new SqlConnection())
            {
                //conn.ConnectionString = "Server=tcp:myjac.database.windows.net,1433;Initial Catalog=myJacDB;Persist Security Info=False;User ID=jing@myjac;Password=1qaz@WSX;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;";
                //conn.ConnectionString = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=\\jacstudents\students\1796111\FirstDB.mdf;Integrated Security=True;Connect Timeout=30";


                conn.ConnectionString = @"Data Source=myjac.database.windows.net;Initial Catalog=myJacDB;User ID=jing;Password=1qaz@WSX";

                conn.Open();

                //SqlCommand comm = new SqlCommand("select * from friends", conn);
                //SqlCommand comm = new SqlCommand("INSERT INTO Friends (name) VALUES (@name)", conn);
                //comm.Parameters.AddWithValue(@"name", "Jerry");
                //comm.ExecuteNonQuery();

                SqlCommand selectCommand = new SqlCommand("SELECT * FROM cars", conn);

                // Create new SqlDataReader object and read data from the command.
                
                using (SqlDataReader reader = selectCommand.ExecuteReader())
                {
                    // while there is another record present
                    while (reader.Read())
                    {
                        // write the data on to the screen
                        MessageBox.Show((String.Format("{0} \t | {1}",
                        // call the objects from their index
                        reader[0], reader[1])));
                    }
                }
                

            }
        }
    }
}
